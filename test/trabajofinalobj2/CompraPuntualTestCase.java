package trabajofinalobj2;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.BeforeEach;

import org.junit.jupiter.api.Test;

import trabajofinalobj2.compras.CompraPuntual;
import trabajofinalobj2.sem.SEM;
import trabajofinalobj2.zonaDeEstacionamiento.PuntoDeVenta;

public class CompraPuntualTestCase {

	private LocalDateTime calendarioSimulado;
	private CompraPuntual compraPuntual;
	private PuntoDeVenta puntoVenta;
	
	@BeforeEach
	public void setUp() {
		calendarioSimulado = LocalDateTime.of(2021, 10, 15, 15, 0);
		puntoVenta = new PuntoDeVenta(new SEM());
		compraPuntual = new CompraPuntual(1, puntoVenta, calendarioSimulado, 2);
	}
	
	@Test
	public void testConstructorDeCompraPuntual() {
		// Se establece un formato para las fechas y horas, 
		// con el finde testearlas de manera individual
		DateTimeFormatter formatoFechas = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		DateTimeFormatter formatoHoras = DateTimeFormatter.ofPattern("HH:mm");
		
		assertEquals(1, compraPuntual.getN�mero());
		assertEquals(puntoVenta, compraPuntual.getPuntoDeVenta());
		assertEquals(calendarioSimulado.format(formatoFechas), compraPuntual.getFecha());
		assertEquals(calendarioSimulado.format(formatoHoras), compraPuntual.getHora());
		assertEquals(2, compraPuntual.getCantDeHoras());
	}
	
	@Test
	public void testLaCompraNoEsUnaCompraRecarga() {
		assertFalse(compraPuntual.esCompraRecarga());
	}

}