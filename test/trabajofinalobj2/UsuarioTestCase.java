package trabajofinalobj2;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import trabajofinalobj2.app.AppUser;
import trabajofinalobj2.app.Caminando;
import trabajofinalobj2.app.ModoAutomático;
import trabajofinalobj2.app.ModoManual;
import trabajofinalobj2.sem.SEM;
import trabajofinalobj2.ubicacion.GPS;
import trabajofinalobj2.ubicacion.Punto;
import trabajofinalobj2.usuario.Automovil;
import trabajofinalobj2.usuario.Celular;
import trabajofinalobj2.usuario.Usuario;
import trabajofinalobj2.zonaDeEstacionamiento.PuntoDeVenta;

public class UsuarioTestCase {

	private Usuario usuario;
	private Celular celular;
	private AppUser app;
	private Automovil auto;
	private SEM sem;
	private PuntoDeVenta puntoDeVenta;
	
	@BeforeEach
	public void setUp() throws Exception {
		sem = new SEM();
		app = new AppUser(sem, new ModoManual(), new Caminando());
		celular = new Celular("1122334455", new GPS(new Punto(1, 1)), app);
		auto = new Automovil("458BGL");
		usuario = new Usuario(auto, celular);
		puntoDeVenta = new PuntoDeVenta(sem);
	}
	
	@Test
	public void testConstructorDeUsuario() {
		assertEquals(auto, usuario.getVehiculo());
		assertEquals(celular, usuario.getCelular());
	}
	
	@Test
	public void testElUsuarioConsultaSuSaldoLuegoDeRegistrarSuCelularElCualEs0() {
		sem.registrarCelularConSuCredito(celular, 0d);
		
		assertEquals(0d, usuario.consultarSaldo());
	}
	
	@Test
	public void testElUsuarioConsultaSuSaldoLuegoDeRecargar200DeCreditoElCualEs200() {
		puntoDeVenta.registrarCompraRecarga(200d, celular, LocalDateTime.of(2021, 10, 15, 15, 0));
		
		assertEquals(200d, usuario.consultarSaldo());
	}
	
	@Test
	public void testElUsuarioConsultaSuSaldoLuegoDeFinalizarUnEstacionamientoDe3HorasElCualEs80() {
		sem.registrarCelularConSuCredito(celular, 200d);
		
		app.realizarRegistroDeEstacionamiento(auto, LocalDateTime.of(2021, 10, 15, 15, 0), celular);
		
		app.realizarFinalizaciónDeEstacionamiento(celular, LocalDateTime.of(2021, 10, 15, 18, 0));
		
		assertEquals(80d, usuario.consultarSaldo());
	}
	
	@Test
	public void testElUsuarioCambiaElModoDeLaAppInstaladaEnSuCelularAModoAutomático() {
		ModoAutomático nuevoModo = new ModoAutomático();
		usuario.cambiarModoDeApp(nuevoModo);
		assertEquals(nuevoModo, app.getModo());
	}

}