package trabajofinalobj2;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import trabajofinalobj2.app.AppUser;
import trabajofinalobj2.app.Caminando;
import trabajofinalobj2.app.ModoAutomático;
import trabajofinalobj2.app.ModoManual;
import trabajofinalobj2.notificaciones.Notificación;
import trabajofinalobj2.sem.SEM;
import trabajofinalobj2.ubicacion.GPS;
import trabajofinalobj2.ubicacion.Punto;
import trabajofinalobj2.ubicacion.ZonaGeográfica;
import trabajofinalobj2.usuario.Automovil;
import trabajofinalobj2.usuario.Celular;
import trabajofinalobj2.usuario.Usuario;
import trabajofinalobj2.zonaDeEstacionamiento.Inspector;
import trabajofinalobj2.zonaDeEstacionamiento.ZonaDeEstacionamiento;

public class ModoAutomáticoTestCase {
	
	private ModoAutomático modo;
	private AppUser app;
	private Celular celular;
	private SEM sem;
	private Automovil auto;
	private Usuario usuario;

	@BeforeEach
	public void setUp() throws Exception {
		sem = new SEM();
		auto = new Automovil("142 TRD");
		app = new AppUser(sem, new ModoManual(), new Caminando());
		celular = new Celular("1144774477", new GPS(new Punto(1,1)), app);
		usuario = new Usuario(auto, celular);
		modo = new ModoAutomático();
	}
	
	@Test
	public void testElModoRegistraUnEstacionamientoAutomaticamenteAlRecibirElMensajeWalkingYLoNotifica() {
		// Para poder iniciar un estacionamiento se debe anteriormente registrar el celular con credito
		// en el sem
		sem.registrarCelularConSuCredito(celular, 200d);
		sem.registrarZonaDeEstacionamiento(new ZonaDeEstacionamiento(new Inspector(), new ZonaGeográfica(new Punto(0,0), 5, 5)));
		
		modo.walking(app);
		
		assertEquals(1, sem.getEstacionamientosVigentes().size());
	}
	
	@Test
	public void testElModoFinalizaUnEstacionamientoAutomaticamenteAlRecibirElMensajeDrivingLuegoDeHaberloRegistradoAnteriormenteYLoNotifica() {
		// Para poder iniciar un estacionamiento se debe anteriormente registrar el celular con credito
		// en el sem
		sem.registrarCelularConSuCredito(celular, 200d);
		sem.registrarZonaDeEstacionamiento(new ZonaDeEstacionamiento(new Inspector(), new ZonaGeográfica(new Punto(0,0), 5, 5)));
		
		modo.walking(app);
		
		modo.driving(app);
		
		assertEquals(0, sem.getEstacionamientosVigentes().size());
		assertEquals(1, sem.getEstacionamientosHistoricos().size());
	}
	
	@Test
	public void testAlModoLeLlegaUnMensajeDeRegistroDeEstacionamientoDelUsuarioYNotificaQueLosRegistrosSeHacenAutomaticamente() {
		Notificación notificación = modo.realizarRegistroDeEstacionamiento(auto, LocalDateTime.of(2021, 11, 5, 13, 0), celular, app);
		
		String notificaciónEsperada = "Los registros y finalizaciones de estacionamiento se hacen automáticamente en este modo.";

		assertEquals(notificaciónEsperada, notificación.showNotification());
	}
	
	@Test
	public void testAlModoLeLlegaUnMensajeDeFinalizaciónDeEstacionamientoDelUsuarioYNotificaQueLasFinalizacionesSeHacenAutomaticamente() {
		Notificación notificación = modo.realizarFinalizaciónDeEstacionamiento(celular, LocalDateTime.of(2021, 11, 5, 15, 0), app);
		
		String notificaciónEsperada = "Los registros y finalizaciones de estacionamiento se hacen automáticamente en este modo.";

		assertEquals(notificaciónEsperada, notificación.showNotification());
	}

}
