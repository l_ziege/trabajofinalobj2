package trabajofinalobj2;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import trabajofinalobj2.app.AppUser;
import trabajofinalobj2.app.Caminando;
import trabajofinalobj2.app.ModoManual;
import trabajofinalobj2.compras.CompraRecarga;
import trabajofinalobj2.sem.SEM;
import trabajofinalobj2.ubicacion.GPS;
import trabajofinalobj2.ubicacion.Punto;
import trabajofinalobj2.usuario.Celular;
import trabajofinalobj2.zonaDeEstacionamiento.PuntoDeVenta;

public class CompraRecargaTestCase {

	private LocalDateTime calendarioSimulado;
	private CompraRecarga recarga;
	private PuntoDeVenta puntoVenta;
	private Celular celular;
	
	@BeforeEach
	public void setUp() {
		calendarioSimulado = LocalDateTime.of(2021, 10, 15, 15, 0);
		puntoVenta = new PuntoDeVenta(new SEM());
		celular = new Celular("154010", new GPS(new Punto(1, 1)), new AppUser(new SEM(), new ModoManual(), new Caminando()));
		recarga = new CompraRecarga(1, puntoVenta, calendarioSimulado, 100.0, celular);
	}
	
	@Test
	public void testConstructorDeCompraRecarga() {
		// Se establece un formato para las fechas y horas, 
		// con el finde testearlas de manera individual
		DateTimeFormatter formatoFechas = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		DateTimeFormatter formatoHoras = DateTimeFormatter.ofPattern("HH:mm");
		
		assertEquals(1, recarga.getN�mero());
		assertEquals(puntoVenta, recarga.getPuntoDeVenta());
		assertEquals(calendarioSimulado.format(formatoFechas), recarga.getFecha());
		assertEquals(calendarioSimulado.format(formatoHoras), recarga.getHora());
		assertEquals(100.0, recarga.getMonto());
		assertEquals(celular, recarga.getCelular());
	}
	
	@Test
	public void testLaCompraEsUnaCompraRecarga() {
		assertTrue(recarga.esCompraRecarga());
	}

}