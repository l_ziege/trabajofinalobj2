package trabajofinalobj2;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import trabajofinalobj2.app.AppUser;
import trabajofinalobj2.app.Caminando;
import trabajofinalobj2.app.ModoManual;
import trabajofinalobj2.estacionamientos.Estacionamiento;
import trabajofinalobj2.notificaciones.Notificación;
import trabajofinalobj2.notificaciones.NotificaciónInicioEstacionamiento;
import trabajofinalobj2.sem.SEM;
import trabajofinalobj2.ubicacion.GPS;
import trabajofinalobj2.ubicacion.Punto;
import trabajofinalobj2.ubicacion.ZonaGeográfica;
import trabajofinalobj2.usuario.Automovil;
import trabajofinalobj2.usuario.Celular;
import trabajofinalobj2.usuario.Usuario;
import trabajofinalobj2.usuario.Vehiculo;
import trabajofinalobj2.zonaDeEstacionamiento.Inspector;
import trabajofinalobj2.zonaDeEstacionamiento.PuntoDeVenta;
import trabajofinalobj2.zonaDeEstacionamiento.ZonaDeEstacionamiento;

class NotificaciónInicioEstacionamientoTestCase {

	private Notificación notificacion;
	private Estacionamiento estacionamiento;
	private LocalDateTime inicio;
	private Vehiculo auto;
	
	private AppUser app;
	private Usuario user;
	private Celular celular;
	private SEM sem;
	private PuntoDeVenta puntoDeVenta;
	
	@BeforeEach
	void setUp() throws Exception {
		auto = new Automovil("ERR 404");
		inicio = LocalDateTime.of(2021, 1, 11, 12, 0);
		
		sem = new SEM();
		app = new AppUser(sem, new ModoManual(), new Caminando());
		celular = new Celular("1144774477", new GPS(new Punto(1,1)), app);
		user = new Usuario(auto, celular);
		puntoDeVenta = new PuntoDeVenta(sem);

	}
	
	@Test
	void testPrintViaApp() {
		ZonaDeEstacionamiento zona = new ZonaDeEstacionamiento(new Inspector(), new ZonaGeográfica(new Punto(0, 0), 5, 5));
		zona.registrarPuntoDeVenta(puntoDeVenta);
		
		puntoDeVenta.registrarCompraRecarga(1500, celular, inicio);
		app.realizarRegistroDeEstacionamiento(auto, inicio, celular);
		estacionamiento = sem.estacionamientoActualConPatente("ERR 404");
		notificacion = new NotificaciónInicioEstacionamiento(estacionamiento);
		
		assertEquals(notificacion.showNotification(), "Estacionamiento iniciado." + "\n"
				+ "Vehiculo Asociado" + "\n"
				+ "Fecha: 11-01-2021" + "\n"
				+ "Inicio: 12-00" + "\n"
				+ "Fin: 20:00");
	}

}
