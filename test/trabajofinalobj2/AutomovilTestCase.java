package trabajofinalobj2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import trabajofinalobj2.usuario.Automovil;

public class AutomovilTestCase {

	private Automovil auto;
	
	@BeforeEach
	public void setup() {
		auto = new Automovil("OBJ 2021");
	}
	
	@Test
	public void testPatente() {
		assertEquals(auto.getPatente(), "OBJ 2021");
	}
	
	@Test
	public void testToString() {
		assertEquals(auto.toString(), "Auto con patente: [OBJ 2021]");
	}

}
