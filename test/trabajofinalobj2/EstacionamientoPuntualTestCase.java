package trabajofinalobj2;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import trabajofinalobj2.app.AppUser;
import trabajofinalobj2.app.Caminando;
import trabajofinalobj2.app.ModoManual;
import trabajofinalobj2.estacionamientos.EstacionamientoPuntual;
import trabajofinalobj2.sem.SEM;
import trabajofinalobj2.ubicacion.GPS;
import trabajofinalobj2.ubicacion.Punto;
import trabajofinalobj2.usuario.Automovil;
import trabajofinalobj2.usuario.Celular;
import trabajofinalobj2.usuario.Vehiculo;

public class EstacionamientoPuntualTestCase {

	private EstacionamientoPuntual estPuntual;
	private LocalDateTime hora;
	private Celular celular;
	private AppUser app;
	private Vehiculo auto;
	
	@BeforeEach
	public void setup() {
		hora = LocalDateTime.of(2020, 6, 30, 10, 45);
		auto = new Automovil("FGR 017");
		estPuntual = new EstacionamientoPuntual(auto, hora, 4);
		
		app = new AppUser(new SEM(), new ModoManual(), new Caminando());
		celular = new Celular("1144774477", new GPS(new Punto(1,1)), app);
	}
	
	@Test
	public void testConstructorDeEstacionamientoPuntual() {
		assertEquals(estPuntual.getHoras(), 4);
		assertEquals(estPuntual.getFecha(), "30-06-2020");
		assertEquals(estPuntual.getHoraInicio(), "10-45");
		assertEquals(estPuntual.getVehiculo(), auto);
	}
	
	@Test
	public void testCosto() {
		assertEquals(estPuntual.costo(), 160.0);
	}
	
	@Test
	public void testMaximoPosible() {
		assertTrue(estPuntual.maximoPosible().isEqual(hora.plusHours(4)));
	}
	
	@Test
	public void testSeRegistroConCelular() {
		assertFalse(estPuntual.esEstacionamientoRegistradoConCelular(celular));
	}
	
	@Test
	public void testElEstacionamientoAunEstaVigente() {
		assertTrue(estPuntual.estaVigente(hora.plusHours(2)));
	}
	
	@Test
	public void testElEstacionamientoNoEstaVigente5HorasDespues() {
		assertFalse(estPuntual.estaVigente(hora.plusHours(5)));
	}

}
