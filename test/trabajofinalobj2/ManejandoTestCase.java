package trabajofinalobj2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import trabajofinalobj2.app.AppUser;
import trabajofinalobj2.app.EstadoMovimiento;
import trabajofinalobj2.app.Manejando;
import trabajofinalobj2.app.ModoManual;
import trabajofinalobj2.sem.SEM;
import trabajofinalobj2.ubicacion.GPS;
import trabajofinalobj2.ubicacion.Punto;
import trabajofinalobj2.usuario.Automovil;
import trabajofinalobj2.usuario.Celular;
import trabajofinalobj2.usuario.Usuario;

public class ManejandoTestCase {

	private Manejando manejando;
	private AppUser app;
	private Celular celular;
	private Automovil auto;
	private Usuario usuario;

	@BeforeEach
	public void setUp() throws Exception {
		manejando = new Manejando();
		app = new AppUser(new SEM(), new ModoManual(), manejando);
		auto = new Automovil("142 TRD");
		celular = new Celular("1144774477", new GPS(new Punto(1,1)), app);
		usuario = new Usuario(auto, celular);
	}

	@Test
	public void testElEstadoDeMovimientoManejandoRecibeElMensajeDrivingYNoHaceNadaLuegoRecibeElMensajeWalkingYCambiaElEstadoDeMovimientoDeLaAppACaminando() {
		app.iniciarSesión(usuario);
		
		manejando.driving(app);
		EstadoMovimiento estadoMovimientoAnterior = app.getEstadoMov();
		manejando.walking(app);
		
		assertNotSame(estadoMovimientoAnterior, app.getEstadoMov());
		assertEquals("Estado movimiento caminando.", app.getEstadoMov().toString());
	}
	
	/*@Test
	public void testRetornoDeEstadoMovimientoCaminandoAlQuererCambiarMovimientoEnElMovimientoManejando() {
		EstadoMovimiento estadoCambiado = manejando.cambiarMovimiento();
		
		assertEquals("Estado movimiento caminando.", estadoCambiado.toString());
	}*/

}
