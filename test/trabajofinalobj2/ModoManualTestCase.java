package trabajofinalobj2;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import trabajofinalobj2.app.AppUser;
import trabajofinalobj2.app.Caminando;
import trabajofinalobj2.app.ModoManual;
import trabajofinalobj2.notificaciones.Notificación;
import trabajofinalobj2.sem.SEM;
import trabajofinalobj2.ubicacion.GPS;
import trabajofinalobj2.ubicacion.Punto;
import trabajofinalobj2.ubicacion.ZonaGeográfica;
import trabajofinalobj2.usuario.Automovil;
import trabajofinalobj2.usuario.Celular;
import trabajofinalobj2.usuario.Usuario;
import trabajofinalobj2.zonaDeEstacionamiento.Inspector;
import trabajofinalobj2.zonaDeEstacionamiento.ZonaDeEstacionamiento;

public class ModoManualTestCase {
	
	private ModoManual modo;
	private AppUser app;
	private Celular celular;
	private SEM sem;
	private Automovil auto;
	private Usuario usuario;

	@BeforeEach
	public void setUp() throws Exception {
		sem = new SEM();
		auto = new Automovil("142 TRD");
		app = new AppUser(sem, new ModoManual(), new Caminando());
		celular = new Celular("1144774477", new GPS(new Punto(1,1)), app);
		modo = new ModoManual();
		usuario = new Usuario(auto, celular);
	}

	@Test
	public void testElUsuarioBajaDelAutoCambiandoSuFormaDeDesplazamientoYSeRegistraUnEstacionamientoRecibiendoUnaNotificaciónDeInicio() {
		// Para iniciar un estacionamiento via app se necesita registrar el celular con credito.
		sem.registrarCelularConSuCredito(celular, 150d);
		sem.registrarZonaDeEstacionamiento(new ZonaDeEstacionamiento(new Inspector(), new ZonaGeográfica(new Punto(5,3), 8, 19)));
		sem.registrarZonaDeEstacionamiento(new ZonaDeEstacionamiento(new Inspector(), new ZonaGeográfica(new Punto(0,0), 5, 5)));
		
		app.iniciarSesión(usuario);
		
		modo.walking(app);
		Notificación notificación = modo.realizarRegistroDeEstacionamiento(auto, LocalDateTime.of(2021, 5, 5, 10, 0), celular, app);
		
		String notificaciónEsperada = "Estacionamiento iniciado.\nVehiculo Asociado\nFecha: 05-05-2021\nInicio: 10-00\nFin: 13:00";

		assertEquals(notificaciónEsperada, notificación.showNotification());
	}
	
	@Test
	public void testElUsuarioCambiaSuFormaDeDesplazamientoSubiendoAlAutoYFinalizaUnEstacionamientoIniciadoAnteriormenteRecibiendoUnaNotificaciónDeFin() {
		// Para iniciar un estacionamiento via app se necesita registrar el celular con credito.
		// Luego se puede iniciar el estacionamiento y posteriormente finalizarlo.
		sem.registrarCelularConSuCredito(celular, 150d);
		
		modo.realizarRegistroDeEstacionamiento(auto, LocalDateTime.of(2021, 5, 5, 10, 0), celular, app);
		
		modo.driving(app);
		Notificación notificación = modo.realizarFinalizaciónDeEstacionamiento(celular, LocalDateTime.of(2021, 5, 5, 12, 0), app);
		
		String notificaciónEsperada = "Estacionamiento finalizado.\nVehiculo Asociado\nFecha: 05-05-2021\nInicio: 10-00\nFin: 12:00\nDuración en horas: 2\nMonto: 80.0";
		
		assertEquals(notificaciónEsperada, notificación.showNotification());
	}
	
	@Test
	public void testSeTrataDeIniciarUnEstacionamientoSinTenerCreditoRecibiendoUnaNotificaciónDeSaldoInsuficiente() {
		sem.registrarCelularConSuCredito(celular, 0d);
		
		Notificación notificación = modo.realizarRegistroDeEstacionamiento(auto, LocalDateTime.of(2021, 5, 5, 10, 0), celular, app);
		
		String notificaciónEsperada = "Saldo insuficiente para iniciar un estacionamiento";
		
		assertEquals(notificaciónEsperada, notificación.showNotification());
	}

}
