package trabajofinalobj2;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import trabajofinalobj2.app.AppUser;
import trabajofinalobj2.app.Caminando;
import trabajofinalobj2.app.ModoManual;
import trabajofinalobj2.estacionamientos.EstacionamientoViaApp;
import trabajofinalobj2.sem.SEM;
import trabajofinalobj2.ubicacion.GPS;
import trabajofinalobj2.ubicacion.Punto;
import trabajofinalobj2.ubicacion.ZonaGeográfica;
import trabajofinalobj2.usuario.Automovil;
import trabajofinalobj2.usuario.Celular;
import trabajofinalobj2.usuario.Usuario;
import trabajofinalobj2.zonaDeEstacionamiento.Inspector;
import trabajofinalobj2.zonaDeEstacionamiento.PuntoDeVenta;
import trabajofinalobj2.zonaDeEstacionamiento.ZonaDeEstacionamiento;

public class EstacionamientoViaAppTestCase {

	private EstacionamientoViaApp estViaApp;
	private LocalDateTime hora;
	private Automovil auto;
	private AppUser app;
	private Usuario user;
	private Celular celular;
	private SEM sem;
	private Celular otroCelular;
	private ZonaDeEstacionamiento zona;
	private PuntoDeVenta puntoDeVenta;
	
	@BeforeEach
	public void setup() {
		hora = LocalDateTime.of(2020, 6, 30, 10, 45);
		auto = new Automovil("ZZZ 000");
		
		sem = new SEM();
		app = new AppUser(sem, new ModoManual(), new Caminando());
		celular = new Celular("1144774477", new GPS(new Punto(1,1)), app);
		user = new Usuario(new Automovil("LFO 675"), celular);
		otroCelular = new Celular("11 2222 4444", new GPS(new Punto(0, 0)), app);		
		estViaApp = new EstacionamientoViaApp(auto, hora, celular);
		puntoDeVenta = new PuntoDeVenta(sem);
		
		zona = new ZonaDeEstacionamiento(new Inspector(), new ZonaGeográfica(new Punto(0, 0), 5, 5));
	}

	@Test
	public void testConstructorDeEstacionamientoViaApp() {
		assertEquals(estViaApp.getFecha(), "30-06-2020");
		assertEquals(estViaApp.getHoraInicio(), "10-45");
		assertEquals(estViaApp.getVehiculo(), auto);
	}
	
	@Test
	public void testEsConCelular() {
		assertTrue(estViaApp.esEstacionamientoRegistradoConCelular(celular));
		assertFalse(estViaApp.esEstacionamientoRegistradoConCelular(otroCelular));
	}
	
	@Test
	public void testCobrarCredito() {
		zona.registrarPuntoDeVenta(puntoDeVenta);
		
		// se registra el estacionamiento
		puntoDeVenta.registrarCompraRecarga(5000, celular, hora);
		sem.registrarEstacionamiento(estViaApp);
		
		// se finaliza el estacionamiento
		LocalDateTime fin = LocalDateTime.of(2020, 6, 30, 13, 30);
		app.realizarFinalizaciónDeEstacionamiento(celular, fin);
		
		assertEquals(celular.consultarSaldo(), 4880);
	}
	
	@Test
	public void testMaximoPosiblePorSaldo() {
		zona.registrarPuntoDeVenta(puntoDeVenta);
		
		puntoDeVenta.registrarCompraRecarga(90, celular, hora);
		sem.registrarEstacionamiento(estViaApp);
		
		assertEquals(estViaApp.getPosibleHoraMáximaFin(), "12:45");
	}
	
	@Test
	public void testMaximoPosiblePorFranjaHoraria() {
		zona.registrarPuntoDeVenta(puntoDeVenta);
		puntoDeVenta.registrarCompraRecarga(10000, celular, hora);
		sem.registrarEstacionamiento(estViaApp);
		
		System.out.println(estViaApp.maximoPosible());
		assertEquals(estViaApp.getPosibleHoraMáximaFin(), "20:00");
	}
	
	@Test
	public void testCosto() {
		zona.registrarPuntoDeVenta(puntoDeVenta);
		
		// se registra el estacionamiento
		puntoDeVenta.registrarCompraRecarga(5000, celular, hora);
		sem.registrarEstacionamiento(estViaApp);
		
		LocalDateTime fin = LocalDateTime.of(2020, 6, 30, 13, 30);
		app.realizarFinalizaciónDeEstacionamiento(celular, fin);
		
		assertEquals(estViaApp.costo(), 120);
	}
}
