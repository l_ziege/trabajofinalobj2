package trabajofinalobj2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import trabajofinalobj2.app.AppUser;
import trabajofinalobj2.app.Caminando;
import trabajofinalobj2.app.ModoManual;
import trabajofinalobj2.sem.CreditoPorCelular;
import trabajofinalobj2.sem.SEM;
import trabajofinalobj2.ubicacion.GPS;
import trabajofinalobj2.ubicacion.Punto;
import trabajofinalobj2.usuario.Celular;

public class CreditoPorCelularTestCase {
	
	private CreditoPorCelular creditoPorCelular;
	private Celular celular;
	
	@BeforeEach
	public void setUp() throws Exception {
		celular = new Celular("1122334455", new GPS(new Punto(1, 1)), new AppUser(new SEM(), new ModoManual(), new Caminando()));
		creditoPorCelular = new CreditoPorCelular(celular, 200d);
	}

	@Test
	public void testConstructorDeCreditoPorCelular() {
		assertEquals(celular, creditoPorCelular.getCelular());
		assertEquals(200d, creditoPorCelular.getCredito());
	}
	
	@Test
	public void testSeLeAcredita150DeCreditoAlCelular() {
		creditoPorCelular.actualizarCredito(150d);
		
		assertEquals(350d, creditoPorCelular.getCredito());
	}
	
	@Test
	public void testAlCelularSeLeRegistraUnGastoDe100() {
		creditoPorCelular.actualizarCredito(-100d);
		
		assertEquals(100d, creditoPorCelular.getCredito());
	}

}
