package trabajofinalobj2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import trabajofinalobj2.notificaciones.Notificación;
import trabajofinalobj2.notificaciones.NotificaciónSaldoInsuficiente;

public class NotificaciónSaldoInsuficienteTestCase {

	private Notificación notificacion;
	
	@BeforeEach
	public void setup() {
		notificacion = new NotificaciónSaldoInsuficiente();
	}
	
	@Test
	public void testPrint() {
		assertEquals(notificacion.showNotification(), "Saldo insuficiente para iniciar un estacionamiento");
	}

}
