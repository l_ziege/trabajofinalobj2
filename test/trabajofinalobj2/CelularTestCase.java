package trabajofinalobj2;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import trabajofinalobj2.app.AppUser;
import trabajofinalobj2.app.Caminando;
import trabajofinalobj2.app.ModoManual;
import trabajofinalobj2.sem.SEM;
import trabajofinalobj2.ubicacion.GPS;
import trabajofinalobj2.ubicacion.Punto;
import trabajofinalobj2.ubicacion.ZonaGeográfica;
import trabajofinalobj2.usuario.Automovil;
import trabajofinalobj2.usuario.Celular;
import trabajofinalobj2.usuario.Usuario;
import trabajofinalobj2.zonaDeEstacionamiento.Inspector;
import trabajofinalobj2.zonaDeEstacionamiento.PuntoDeVenta;
import trabajofinalobj2.zonaDeEstacionamiento.ZonaDeEstacionamiento;

public class CelularTestCase {

	private Celular celular;
	private AppUser app;
	private Usuario usuario;
	private SEM sem;
	private ZonaDeEstacionamiento zona;
	private PuntoDeVenta puntoDeVenta;
	private LocalDateTime fechaYHoraSimulada;
	private GPS gps;
	
	@BeforeEach
	public void setUp() throws Exception {
		sem = new SEM();
		app = new AppUser(sem, new ModoManual(), new Caminando());
		gps = new GPS(new Punto(1, 1));
		celular = new Celular("1122554433", gps, app);
		usuario = new Usuario(new Automovil("145 ABC"), celular);
		zona = new ZonaDeEstacionamiento(new Inspector(), new ZonaGeográfica(new Punto(0, 0), 5, 5));
		puntoDeVenta = new PuntoDeVenta(sem);
		fechaYHoraSimulada = LocalDateTime.of(2020, 12, 15, 16, 0);
	}
	
	@Test
	public void testConstructorDeCelular() {
		assertEquals(app, celular.getApp());
		assertEquals(gps, celular.getGps());
		assertEquals("1122554433", celular.getNúmero());
	}

	@Test
	public void testElSaldoInicialDeUnCelularRegistradoEs0() {
		sem.registrarCelularConSuCredito(celular, 0d);
		assertEquals(0, celular.consultarSaldo());
	}
	
	@Test
	public void testElSaldoDeUnCelularEs200LuegoDeRealizarleUnaRecargaDe200() {
		zona.registrarPuntoDeVenta(puntoDeVenta);
		
		puntoDeVenta.registrarCompraRecarga(200, celular, fechaYHoraSimulada);
		assertEquals(200, celular.consultarSaldo());
	}
	
	@Test
	public void testElSaldoDeUnCelularConRecargaDe200EsDe160LuegoDePagarUnEstacionamientoDe1Hora() {
		zona.registrarPuntoDeVenta(puntoDeVenta);
		
		puntoDeVenta.registrarCompraRecarga(200, celular, fechaYHoraSimulada);
		
		app.realizarRegistroDeEstacionamiento(usuario.getVehiculo(), fechaYHoraSimulada, celular);
		app.realizarFinalizaciónDeEstacionamiento(celular, fechaYHoraSimulada.plusHours(1));
		assertEquals(160, celular.consultarSaldo());
	}
	
}
