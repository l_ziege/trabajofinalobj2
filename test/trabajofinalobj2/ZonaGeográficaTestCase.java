package trabajofinalobj2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import trabajofinalobj2.app.AppUser;
import trabajofinalobj2.app.Caminando;
import trabajofinalobj2.app.ModoManual;
import trabajofinalobj2.sem.SEM;
import trabajofinalobj2.ubicacion.GPS;
import trabajofinalobj2.ubicacion.Punto;
import trabajofinalobj2.ubicacion.ZonaGeográfica;
import trabajofinalobj2.usuario.Celular;

public class ZonaGeográficaTestCase {

	private ZonaGeográfica zona;
	private AppUser app;
	private Celular celular;
	
	@BeforeEach
	public void setUp() throws Exception {
		zona = new ZonaGeográfica((new Punto(5, 0)), 10, 20);
		app = new AppUser(new SEM(), new ModoManual(), new Caminando());
	}

	@Test
	public void testConstructorDeZonaGeográfica() {
		assertEquals(zona.getLongitud(), 10);
		assertEquals(zona.getAltura(), 20);
	}
	
	@Test
	public void testLosPuntosPertenecenALaZona() {
		GPS gps = new GPS(new Punto(15, 5));
		celular = new Celular("11 4562 0232", gps, app);
		assertTrue(zona.incluyeElPunto(celular.getGps().getPunto()));
	}
	
	@Test
	public void testLosPuntosNoPertenecenALaZona() {
		// no pertenece X, pero si Y
		GPS gps = new GPS(new Punto(0, 20));
		celular = new Celular("11 4562 0232", gps, app);
		assertFalse(zona.incluyeElPunto(celular.getGps().getPunto()));
		
		// pertenece X, pero no Y
		gps = new GPS(new Punto(9, 30));
		celular = new Celular("11 4562 0232", gps, app);
		assertFalse(zona.incluyeElPunto(celular.getGps().getPunto()));
		
		// no pertenece ni X ni Y
		gps = new GPS(new Punto(0, -2));
		celular = new Celular("11 4562 0232", gps, app);
		assertFalse(zona.incluyeElPunto(celular.getGps().getPunto()));
	}
	
	@Test
	public void testToString() {
		String s = "Zona Geografica entre Punto (5.0, 0.0) y Punto (15.0, 20.0)";
		assertEquals(zona.toString(), s);
	}

}
