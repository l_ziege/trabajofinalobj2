package trabajofinalobj2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import trabajofinalobj2.sem.SEM;
import trabajofinalobj2.ubicacion.Punto;
import trabajofinalobj2.ubicacion.ZonaGeográfica;
import trabajofinalobj2.zonaDeEstacionamiento.Inspector;
import trabajofinalobj2.zonaDeEstacionamiento.PuntoDeVenta;
import trabajofinalobj2.zonaDeEstacionamiento.ZonaDeEstacionamiento;

public class ZonaDeEstacionamientoTestCase {

	private ZonaDeEstacionamiento zona;
	private PuntoDeVenta puntoVenta;
	private Inspector inspector;
	
	@BeforeEach
	public void setup() {
		puntoVenta = new PuntoDeVenta(new SEM());
		inspector = new Inspector();
		
		zona = new ZonaDeEstacionamiento(inspector, new ZonaGeográfica(new Punto(0, 0), 5, 5));
	}
	
	@Test
	public void nuevaZona() {
		assertEquals(zona.getInspector(), inspector);
		assertEquals(zona.getPuntos().size(), 0);
	}
	
	@Test
	public void testLaZonaRegistraUnPunto() {
		assertEquals(zona.getPuntos().size(), 0);
		zona.registrarPuntoDeVenta(puntoVenta);
		assertEquals(zona.getPuntos().size(), 1);
		assertTrue(zona.getPuntos().contains(puntoVenta));
	}
	
	@Test
	public void testLaZonaEliminaUnPunto() {
		zona.registrarPuntoDeVenta(puntoVenta);
		assertEquals(zona.getPuntos().size(), 1);
		assertTrue(zona.getPuntos().contains(puntoVenta));
		
		zona.retirarPuntoDeVenta(puntoVenta);
		assertEquals(zona.getPuntos().size(), 0);
		assertFalse(zona.getPuntos().contains(puntoVenta));
	}
	
	@Test
	public void testLaZonaConoceElPuntoMasCercano() {
		PuntoDeVenta punto = new PuntoDeVenta(new SEM());
		zona.registrarPuntoDeVenta(puntoVenta);
		zona.registrarPuntoDeVenta(punto);
		
		assertEquals(zona.puntoDeVentaMasCercano(), puntoVenta);
		
		zona.retirarPuntoDeVenta(puntoVenta);
		assertEquals(zona.puntoDeVentaMasCercano(), punto);
	}
	
	@Test
	public void testPoseePunto() {
		assertTrue(zona.estaEnZona(new Punto(2, 1)));
		assertFalse(zona.estaEnZona(new Punto(10, -9)));
	}

}
