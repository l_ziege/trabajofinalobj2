package trabajofinalobj2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import trabajofinalobj2.ubicacion.Punto;

public class PuntoTestCase {

	private Punto punto;
	
	@BeforeEach
	public void setUp() throws Exception {
		punto = new Punto(3.14, 7.6);
	}

	@Test
	public void testConstructorDePunto() {
		assertEquals(punto.getX(), 3.14);
		assertEquals(punto.getY(), 7.6);
	}

	@Test
	public void testToString() {
		assertEquals(punto.toString(), "Punto (3.14, 7.6)");
	}
}
