package trabajofinalobj2;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import trabajofinalobj2.app.AppUser;
import trabajofinalobj2.app.Caminando;
import trabajofinalobj2.app.ModoManual;
import trabajofinalobj2.sem.SEM;
import trabajofinalobj2.ubicacion.GPS;
import trabajofinalobj2.ubicacion.Punto;
import trabajofinalobj2.usuario.Automovil;
import trabajofinalobj2.usuario.Celular;
import trabajofinalobj2.zonaDeEstacionamiento.PuntoDeVenta;

public class PuntoDeVentaTestCase {

	private PuntoDeVenta punto;
	private SEM sem;
	private Celular celular;
	
	@BeforeEach
	public void setup() {
		sem = new SEM();
		punto = new PuntoDeVenta(sem);
		celular = new Celular("1122554433", new GPS(new Punto(1, 1)), new AppUser(sem, new ModoManual(), new Caminando()));
	}
	
	@Test
	public void testElPuntoDeVentaRegistraUnEstacionamientoYUnaCompraEnElSem() {
		punto.registrarEstacionamientoYCompra(new Automovil("142 AGD"), LocalDateTime.of(2021, 5, 20, 14, 0), 1);
		
		assertEquals(1, sem.getCompras().size());
		assertEquals(1, sem.getEstacionamientosVigentes().size());
	}
	
	@Test
	public void testElPuntoRegistraUnaCompraRecargaDe100DeCreditoEnElSem() {
		sem.registrarCelularConSuCredito(celular, 0d);
		
		punto.registrarCompraRecarga(100d, celular, LocalDateTime.of(2021, 5, 20, 14, 0));
		
		assertEquals(1, sem.getCompras().size());
		assertEquals(100d, sem.getCreditoDeCelular(celular));
	}

}
