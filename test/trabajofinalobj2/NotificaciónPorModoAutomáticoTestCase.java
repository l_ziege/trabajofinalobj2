package trabajofinalobj2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import trabajofinalobj2.notificaciones.NotificaciónPorModoAutomático;

public class NotificaciónPorModoAutomáticoTestCase {

	private NotificaciónPorModoAutomático notificación;
	
	@BeforeEach
	public void setUp() throws Exception {
		notificación = new NotificaciónPorModoAutomático();
	}

	@Test
	public void testPrint() {
		assertEquals("Los registros y finalizaciones de estacionamiento se hacen automáticamente en este modo.", notificación.showNotification());
	}

}
