package trabajofinalobj2;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import trabajofinalobj2.sem.Infracción;
import trabajofinalobj2.ubicacion.Punto;
import trabajofinalobj2.ubicacion.ZonaGeográfica;
import trabajofinalobj2.usuario.Automovil;
import trabajofinalobj2.usuario.Vehiculo;
import trabajofinalobj2.zonaDeEstacionamiento.Inspector;
import trabajofinalobj2.zonaDeEstacionamiento.ZonaDeEstacionamiento;

public class InfracciónTestCase {

	private Infracción infraccion;
	private Inspector inspector;
	private ZonaDeEstacionamiento zona;
	private LocalDateTime calendario;
	private Vehiculo auto;
	
	@BeforeEach
	public void setup() {
		inspector = new Inspector();
		zona = new ZonaDeEstacionamiento(inspector, new ZonaGeográfica(new Punto(0, 0), 5, 5));
		calendario = LocalDateTime.of(2021, 10, 28, 18, 30);
		auto = new Automovil("ABC 123");
		
		infraccion = new Infracción(auto, inspector, zona, calendario);
	}
	
	@Test
	public void testConstructorDeInfracción() {
		assertEquals(infraccion.getInspector(), inspector);
		assertEquals(infraccion.getVehiculo(), auto);
		assertEquals(infraccion.getZona(), zona);
		
		assertEquals(infraccion.getFecha(), "28-10-2021");
		assertEquals(infraccion.getHora(), "18:30");
	}
	
	@Test
	public void testToString() {
		String s = "# INFRACCION" + "\n" 
				+ "Vehiculo multado: Auto con patente: [ABC 123]"  + "\n" 
				+ "Emitida el: 28-10-2021 a las: 18:30";
		assertEquals(infraccion.toString(), s);
	}

}
