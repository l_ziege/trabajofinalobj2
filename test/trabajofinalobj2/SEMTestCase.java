package trabajofinalobj2;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import trabajofinalobj2.app.AppUser;
import trabajofinalobj2.app.Caminando;
import trabajofinalobj2.app.ModoManual;
import trabajofinalobj2.compras.CompraPuntual;
import trabajofinalobj2.compras.CompraRecarga;
import trabajofinalobj2.estacionamientos.EstacionamientoPuntual;
import trabajofinalobj2.estacionamientos.EstacionamientoViaApp;
import trabajofinalobj2.sem.Entidad;
import trabajofinalobj2.sem.SEM;
import trabajofinalobj2.ubicacion.GPS;
import trabajofinalobj2.ubicacion.Punto;
import trabajofinalobj2.ubicacion.ZonaGeográfica;
import trabajofinalobj2.usuario.Automovil;
import trabajofinalobj2.usuario.Celular;
import trabajofinalobj2.zonaDeEstacionamiento.Inspector;
import trabajofinalobj2.zonaDeEstacionamiento.PuntoDeVenta;
import trabajofinalobj2.zonaDeEstacionamiento.ZonaDeEstacionamiento;

public class SEMTestCase {
	
	private SEM sem;
	private Celular celular;
	private AppUser app;
	
	@BeforeEach
	public void setUp() throws Exception {
		sem = new SEM();
		app = new AppUser(sem, new ModoManual(), new Caminando());
		celular = new Celular("1145784578", new GPS(new Punto(1, 1)), app);
	}

	@Test
	public void testUnSemRecienCreadoNoTieneZonasDeEstacionamientos() {
		assertTrue(sem.getZonasDeEstacionamiento().isEmpty());
	}
	
	@Test
	public void testUnSemRecienCreadoSeLeAgregaUnaZonaDeEstacionamientoUnaUnicaVez() {
		ZonaDeEstacionamiento zona = new ZonaDeEstacionamiento(new Inspector(),new ZonaGeográfica(new Punto(0, 0), 5, 5));
		sem.registrarZonaDeEstacionamiento(zona);
		sem.registrarZonaDeEstacionamiento(zona);
		assertEquals(1, sem.getZonasDeEstacionamiento().size());
	}
	
	@Test
	public void testUnSemRecienCreadoNoTieneCompras() {
		assertTrue(sem.getCompras().isEmpty());
	}
	
	@Test
	public void testUnSemRecienCreadoSeLeAgregaUnaCompraPuntualUnaUnicaVez() {
		CompraPuntual compra = new CompraPuntual(1, new PuntoDeVenta(sem), LocalDateTime.of(2021, 10, 15, 15, 0), 2);
		sem.registrarCompra(compra);
		sem.registrarCompra(compra);
		assertEquals(1, sem.getCompras().size());
	}
	
	@Test
	public void testElSiguienteNúmeroDeCompraCuandoElSemNoTieneComprasRegistradasEs1() {
		assertEquals(1, sem.siguienteNúmeroDeCompra());
	}
	
	@Test
	public void testUnSemRecienCreadoNoTieneEstacionamientosVigentes() {
		assertTrue(sem.getEstacionamientosVigentes().isEmpty());
	}
	
	@Test
	public void testUnSemRecienCreadoSeLeAgregaUnEstacionamientoPuntualUnaUnicaVez() {
		EstacionamientoPuntual estacionamiento = new EstacionamientoPuntual(new Automovil("124FDG"), LocalDateTime.of(2021, 10, 15, 15, 0), 3);
		sem.registrarEstacionamiento(estacionamiento);
		sem.registrarEstacionamiento(estacionamiento);
		assertEquals(1, sem.getEstacionamientosVigentes().size());
	}
	
	@Test
	public void testUnSemRecienCreadoNoTieneRegistradosCelularesConSusCreditos() {
		assertTrue(sem.getCreditosPorCelular().isEmpty());
	}
	
	@Test
	public void testUnSemRecienCreadoSeLeAgregaUnCelularCon0DeCreditoUnaUnicaVez() {
		sem.registrarCelularConSuCredito(celular, 0d);
		sem.registrarCelularConSuCredito(celular, 0d);
		assertEquals(1, sem.getCreditosPorCelular().size());
	}
	
	@Test
	public void testUnSemRecienCreadoNoTieneInfraccionesRegistradas() {
		assertTrue(sem.getInfracciones().isEmpty());
	}

	@Test
	public void testUnSemRecienCreadoSeLeRegistraUnaInfracción() {
		sem.registrarEstacionamiento(new EstacionamientoPuntual(new Automovil("142 GDS"), LocalDateTime.of(2021, 10, 15, 10, 0), 3));
		sem.levantarInfracciónSiCorresponde("142 GDS", LocalDateTime.of(2021, 10, 15, 15, 0), (new ZonaDeEstacionamiento(new Inspector(),new ZonaGeográfica(new Punto(0, 0), 5, 5))));
		assertEquals(1, sem.getInfracciones().size());
	}
	
	@Test
	public void testUnSemRecienCreadoNoTieneEstacionamientosHistoricos() {
		assertTrue(sem.getEstacionamientosHistoricos().isEmpty());
	}
	
	@Test
	public void testElSemFinalizaTodosLosEstacionamientosActualesPorFinDeFranjaHoraria() {
		sem.registrarEstacionamiento(new EstacionamientoPuntual(new Automovil("182 AAA"), LocalDateTime.of(2021, 10, 15, 15, 0), 5));
		sem.registrarEstacionamiento(new EstacionamientoPuntual(new Automovil("352 BZZ"), LocalDateTime.of(2021, 10, 15, 16, 0), 4));
		sem.registrarEstacionamiento(new EstacionamientoPuntual(new Automovil("884 RFD"), LocalDateTime.of(2021, 10, 15, 18, 0), 2));
		sem.finalizarEstacionamientosPorFinDeFranjaHoraria(LocalDateTime.of(2021, 10, 15, 20, 0));
		assertEquals(0, sem.getEstacionamientosVigentes().size());
		assertEquals(3, sem.getEstacionamientosHistoricos().size());
	}
	
	@Test
	public void testElSemActualizaElCreditoDeUnCelularRegistradoAnteriormenteQuedandoSuCreditoActualEn180() {
		Celular otroCelular = new Celular("1155477455", new GPS(new Punto(1, 1)), app);
		sem.registrarCelularConSuCredito(otroCelular, 15d);
		sem.registrarCelularConSuCredito(celular, 100d);
		sem.actualizarCredito(celular, 80d);
		assertEquals(180d, sem.getCreditoDeCelular(celular));
	}
	
	@Test
	public void testElSemFinalizaUnEstacionamientoViaAppIniciadoAnteriormente() {
		sem.registrarCelularConSuCredito(celular, 300d);
		Celular otroCelular = new Celular("1155477455", new GPS(new Punto(1, 1)), app);
		sem.registrarCelularConSuCredito(otroCelular, 100d);
		sem.registrarEstacionamiento(new EstacionamientoViaApp(new Automovil("421, AFD"), LocalDateTime.of(2021, 10, 15, 10, 0), otroCelular));
		sem.registrarEstacionamiento(new EstacionamientoViaApp(new Automovil("999, ABC"), LocalDateTime.of(2021, 10, 15, 11, 0), celular));
		sem.finalizarEstacionamientoViaApp(celular, LocalDateTime.of(2021, 10, 15, 13, 0));
		assertEquals(1, sem.getEstacionamientosVigentes().size());
		assertEquals(1, sem.getEstacionamientosHistoricos().size());
	}
	
	@Test
	public void testElSemFinalizaUnEstacionamientoPuntualIniciadoAnteriormente() {
		EstacionamientoPuntual estacionamiento = new EstacionamientoPuntual(new Automovil("741 RTY"), LocalDateTime.of(2021, 10, 15, 12, 0), 4);
		sem.registrarEstacionamiento(estacionamiento);
		sem.finalizarEstacionamientoPuntual(estacionamiento, LocalDateTime.of(2021, 10, 15, 16, 0));
		assertEquals(0, sem.getEstacionamientosVigentes().size());
		assertEquals(1, sem.getEstacionamientosHistoricos().size());
	}
	
	@Test
	public void testElPrecioPorHoraDeEstacionamientoDelSemEsDe40() {
		assertEquals(40d, sem.precioPorHora());
	}
	
	@Test
	public void testElSemNoTieneRegistradoUnCelularDadoEnElSistema() {
		assertFalse(sem.estaRegistradoElCelular(celular));
	}
	
	@Test
	public void testElSemTieneRegistradoUnCelularDadoRegistradoAnteriormenteEnElSistema() {
		sem.registrarCelularConSuCredito(celular, 0d);
		assertTrue(sem.estaRegistradoElCelular(celular));
	}
	
	@Test
	public void testElSemNoTieneRegistradoUnEsEstacionamientoConPatente144ABG() {
		assertFalse(sem.existeEstacionamientoConPatente("144 ABG"));
	}
	
	@Test
	public void testElSemTieneRegistradoUnEstacionamientoConPatente144ABGRegistradoAnteriormente() {
		sem.registrarEstacionamiento(new EstacionamientoPuntual(new Automovil("222 HHH"), LocalDateTime.of(2021, 10, 15, 10, 0), 3));
		sem.registrarEstacionamiento(new EstacionamientoPuntual(new Automovil("144 ABG"), LocalDateTime.of(2021, 10, 15, 12, 0), 4));
		assertTrue(sem.existeEstacionamientoConPatente("144 ABG"));
	}
	
	@Test
	public void testElSemRetornaElEstacionamientoActualRegistradoConLaPatente144ABG() {
		EstacionamientoPuntual estacionamiento = new EstacionamientoPuntual(new Automovil("144 ABG"), LocalDateTime.of(2021, 10, 15, 12, 0), 4);
		sem.registrarEstacionamiento(new EstacionamientoPuntual(new Automovil("222 HHH"), LocalDateTime.of(2021, 10, 15, 10, 0), 3));
		sem.registrarEstacionamiento(estacionamiento);
		assertEquals(estacionamiento, sem.estacionamientoActualConPatente("144 ABG"));
	}
	
	@Test
	public void testAgregadoDeEntidadEnElSemYVerificaciónDeSusNotificacionesRecibidasMedianteMockito() {
		Entidad entidad = Mockito.mock(Entidad.class);
		sem.registrarObserver(entidad);
		assertEquals(1, sem.getObservadores().size());
		Mockito.verify(entidad).registrarObservable(sem);
		Mockito.verify(entidad, Mockito.never()).getSEM();
		
		CompraRecarga compra = new CompraRecarga(1, new PuntoDeVenta(sem), LocalDateTime.of(2021, 10, 15, 10, 0), 200, celular);
		sem.registrarCompra(compra);
		Mockito.verify(entidad).nuevaCompraRegistrada(sem, compra);
		
		EstacionamientoPuntual estacionamiento = new EstacionamientoPuntual(new Automovil("142 GDS"), LocalDateTime.of(2021, 10, 15, 10, 0), 3);
		sem.registrarEstacionamiento(estacionamiento);
		Mockito.verify(entidad).nuevoEstacionamientoRegistrado(sem, estacionamiento);
		
		sem.finalizarEstacionamientoPuntual(estacionamiento, LocalDateTime.of(2021, 10, 15, 13, 0));
		Mockito.verify(entidad).estacionamientoFinalizado(sem, estacionamiento);
		
		sem.eliminarObserver(entidad);
		assertEquals(0, sem.getObservadores().size());
	}
}
