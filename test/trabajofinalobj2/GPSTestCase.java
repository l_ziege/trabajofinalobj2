package trabajofinalobj2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import trabajofinalobj2.ubicacion.GPS;
import trabajofinalobj2.ubicacion.Punto;

public class GPSTestCase {
	
	private GPS gps;
	private Punto puntoGeografico;
	
	@BeforeEach
	public void setUp() throws Exception {
		puntoGeografico = new Punto(1.5, 2.33);
		gps = new GPS(puntoGeografico);
	}

	@Test
	public void testCoordenadasGPS() {
		assertEquals(gps.getPunto().getX(), 1.5);
		assertEquals(gps.getPunto().getY(), 2.33);
	}

	@Test
	public void testCambiarCoordenadasGPS() {
		assertEquals(gps.getPunto().getX(), 1.5);
		assertEquals(gps.getPunto().getY(), 2.33);
		
		// Se modifica la posicion actual del gps
		gps.actualizarPosición(new Punto(3, 14));
		assertEquals(gps.getPunto().getX(), 3);
		assertEquals(gps.getPunto().getY(), 14);
	}
	
	@Test
	public void testToString() {
		String s = "GPS con ubicación en: Punto (1.5, 2.33)";
		assertEquals(gps.toString(), s);
	}
	
}
