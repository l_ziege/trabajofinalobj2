package trabajofinalobj2;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import trabajofinalobj2.app.AppUser;
import trabajofinalobj2.app.Caminando;
import trabajofinalobj2.app.EstrategiaModoApp;
import trabajofinalobj2.app.ModoAutomático;
import trabajofinalobj2.app.ModoManual;
import trabajofinalobj2.notificaciones.Notificación;
import trabajofinalobj2.sem.SEM;
import trabajofinalobj2.ubicacion.GPS;
import trabajofinalobj2.ubicacion.Punto;
import trabajofinalobj2.usuario.Automovil;
import trabajofinalobj2.usuario.Celular;
import trabajofinalobj2.usuario.Usuario;

public class AppUserTestCase {
	
	private AppUser app;
	private SEM sem;
	private Usuario usuario;
	private Celular celular;
	private Automovil auto;

	@BeforeEach
	public void setUp() throws Exception {
		sem = new SEM();
		auto = new Automovil("142 TRD");
		app = new AppUser(sem, new ModoManual(), new Caminando());
		celular = new Celular("1144774477", new GPS(new Punto(1,1)), app);
		usuario = new Usuario(auto, celular);
	}

	@Test
	public void testConstructorDeAppUser() {
		assertEquals(usuario, app.getUsuarioRegistrado());
		assertEquals(sem, app.getSem());
	}
	
	@Test
	public void testElUsuarioHomeroIniciaSesiónEnLaApp() {
		Usuario homero = new Usuario(new Automovil("222 AAA"), new Celular("1177775555", new GPS(new Punto(1,1)), new AppUser(sem, new ModoManual(), new Caminando())));
		
		app.iniciarSesión(homero);
		
		assertEquals(homero, app.getUsuarioRegistrado());
	}
	
	@Test
	public void testElUsuarioBajaDelAutoCambiandoSuFormaDeDesplazamientoYSeRegistraUnEstacionamientoRecibiendoUnaNotificaciónDeInicio() {
		// Para iniciar un estacionamiento via app se necesita registrar el celular con credito.
		sem.registrarCelularConSuCredito(celular, 100d);
		
		app.walking();
		Notificación notificación = app.realizarRegistroDeEstacionamiento(auto, LocalDateTime.of(2021, 11, 5, 13, 0), celular);
		
		String notificaciónEsperada = "Estacionamiento iniciado.\nVehiculo Asociado\nFecha: 05-11-2021\nInicio: 13-00\nFin: 15:00";

		assertEquals(notificaciónEsperada, notificación.showNotification());
	}
	
	@Test
	public void testElUsuarioCambiaSuFormaDeDesplazamientoSubiendoAlAutoYFinalizaUnEstacionamientoIniciadoAnteriormenteRecibiendoUnaNotificaciónDeFin() {
		// Para iniciar un estacionamiento via app se necesita registrar el celular con credito.
		// Luego se puede iniciar el estacionamiento y posteriormente finalizarlo.
		sem.registrarCelularConSuCredito(celular, 120d);
		
		app.realizarRegistroDeEstacionamiento(auto, LocalDateTime.of(2021, 11, 5, 13, 0), celular);
		
		app.driving();
		Notificación notificación = app.realizarFinalizaciónDeEstacionamiento(celular, LocalDateTime.of(2021, 11, 5, 15, 0));
		
		String notificaciónEsperada = "Estacionamiento finalizado.\nVehiculo Asociado\nFecha: 05-11-2021\nInicio: 13-00\nFin: 15:00\nDuración en horas: 2\nMonto: 80.0";
		
		assertEquals(notificaciónEsperada, notificación.showNotification());
	}
	
	@Test
	public void testDesdeLaAppSeTrataDeIniciarUnEstacionamientoSinTenerCreditoRecibiendoUnaNotificaciónDeSaldoInsuficiente() {
		sem.registrarCelularConSuCredito(celular, 0d);
		
		Notificación notificación = app.realizarRegistroDeEstacionamiento(auto, LocalDateTime.of(2021, 11, 5, 13, 0), celular);
		
		String notificaciónEsperada = "Saldo insuficiente para iniciar un estacionamiento";
		
		assertEquals(notificaciónEsperada, notificación.showNotification());
	}
	
	@Test
	public void testEnLaAppSeCambiaElModoDeManualAAutomatico() {
		EstrategiaModoApp modoAnterior = app.getModo();
		app.cambiarModo(new ModoAutomático());
		
		assertNotSame(modoAnterior.toString(), app.getModo().toString());
	}
	
	@Test
	public void testLaAppConsultaElSaldoDeUnCelularAnteriormenteRegistradoEnElSem() {
		sem.registrarCelularConSuCredito(celular, 20d);
		
		assertEquals(20d, app.consultarSaldoDeCelular(celular));
	}

}
