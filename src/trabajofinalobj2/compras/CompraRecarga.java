package trabajofinalobj2.compras;

import java.time.LocalDateTime;

import trabajofinalobj2.usuario.Celular;
import trabajofinalobj2.zonaDeEstacionamiento.PuntoDeVenta;

/**
 * Subclase de Compra (Abstracta), hereda su comportamiento, 
 * ademas de poseer metodos y atributos que la distinguen del resto de subclases de Compra.
 */

public class CompraRecarga extends Compra {
	
	private double monto;
	private Celular celular;

	/**
	 * Crea una compra recarga.
	 * @param numero El n�mero de la compra.
	 * @param punto El punto de venta donde se realiz� la compra.
	 * @param calendario La fecha y hora en la que se realiz� la compra.
	 * @param monto El monto a recargar en el celular.
	 * @param celular El celular a hacer la recarga.
	 */
	public CompraRecarga(int numero, PuntoDeVenta punto, LocalDateTime calendario, double monto, Celular celular) {
		super();
		this.n�meroDeControl = numero;
		this.puntoVenta = punto;
		this.fechaYHora = calendario;
		this.monto = monto;
		this.celular = celular;
	}

	/**
	 * Describe si la compra es una compra recarga.
	 * @return Verdadero.
	 */
	@Override
	public boolean esCompraRecarga() {
		return true;
	}
	
	/**
	 * Describe el monto a recargar en el celular.
	 * @return El monto a recargar en el celular.
	 */
	public double getMonto() {
		return this.monto;
	}
	
	/** 
	 * Describe el celular a recargar credito.
	 * @return El celular a recargar credito.
	 */
	public Celular getCelular() {
		return this.celular;
	}

}
