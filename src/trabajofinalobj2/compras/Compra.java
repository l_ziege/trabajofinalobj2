package trabajofinalobj2.compras;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import trabajofinalobj2.zonaDeEstacionamiento.PuntoDeVenta;

/**
 * Posee el comportamiento que comparten sus subclases: Compra Puntual y Compra Recarga.
 */

public abstract class Compra {
	
	protected int n�meroDeControl;
	protected PuntoDeVenta puntoVenta;
	protected LocalDateTime fechaYHora;
	
	/**
	 * Describe el n�mero de la compra.
	 * @return El n�mero de la compra.
	 */
	public int getN�mero() {
		return n�meroDeControl;
	}
	
	/**
	 * Describe la fecha en la que se realiz� la compra.
	 * @return La fecha en la que se realiz� la compra.
	 */
	public String getFecha() {
		// Con este formatoFechas se consigue unicamente la fecha del LocalDateTime
		// El formato es dia - mes - a�o
		DateTimeFormatter formatoFechas = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		
		return this.fechaYHora.format(formatoFechas);
	}
	
	/**
	 * Describe la hora en la que se realiz� la compra.
	 * @return La hora en la que se realiz� la compra.
	 */
	public String getHora() {
		// Con este formatoHoras se consigue unicamente la fecha del LocalDateTime
		// El formato es horas : minutos (en 24 horas)
		DateTimeFormatter formatoHoras = DateTimeFormatter.ofPattern("HH:mm");
		
		return this.fechaYHora.format(formatoHoras);
	}
	
	/**
	 * Describe el punto de venta en el que se realiz� la compra.
	 * @return El punto de venta en el que se realiz� la compra.
	 */
	public PuntoDeVenta getPuntoDeVenta() {
		return this.puntoVenta;
	}

	/**
	 * Describe si la compra es una compra recarga, por default es falso.
	 * @return Falso.
	 */
	public boolean esCompraRecarga() {
		return false;
	}
	
}
