package trabajofinalobj2.compras;

import java.time.LocalDateTime;

import trabajofinalobj2.zonaDeEstacionamiento.PuntoDeVenta;

/**
 * Subclase de Compra (Abstracta), hereda su comportamiento, 
 * ademas de poseer metodos y atributos que la distinguen del resto de subclases de Compra.
 */

public class CompraPuntual extends Compra{

	private int cantidadDeHoras;
	
	/**
	 * Crea una compra puntual.
	 * @param n�mero El n�mero de la compra.
	 * @param punto El punto de venta donde se realiz� la compra.
	 * @param calendario Una fecha y hora en la que se realiz� la compra.
	 * @param horas La cantidad de horas por las que se compr� un estacionamiento.
	 */
	public CompraPuntual(int n�mero, PuntoDeVenta punto, LocalDateTime calendario, int horas) {
		this.n�meroDeControl = n�mero;
		this.puntoVenta = punto;
		this.fechaYHora = calendario;
		this.cantidadDeHoras = horas;
	}
	
	/**
	 * Describe la cantidad de horas por las que se compr� un estacionamiento.
	 * @return La cantidad de horas por las que se compr� un estacionamiento.
	 */
	public int getCantDeHoras() {
		return this.cantidadDeHoras;
	}
	
}
