package trabajofinalobj2.ubicacion;

/**
 * Representa el sistema de GPS que poseerá el celular para conocer su ubicación.
 */

public class GPS {
	
	private Punto punto;
	
	/**
	 * Crea un gps.
	 * @param puntoGeográfico El punto actual del gps.
	 */
	public GPS(Punto puntoGeográfico) {
		this.punto = puntoGeográfico;
	}
	
	/**
	 * Describe la posicion (x, y) del gps
	 * @return un Punto (x, y)
	 */
	public Punto getPunto() {
		return this.punto;
	}
	
	/**
	 * Actualiza la posición actual del gps
	 * @param punto La nueva coordenada del gps
	 */
	public void actualizarPosición(Punto punto) {
		this.punto = punto;
	}
	
	/**
	 * Sobreescritura del metodo para representar mejor el gps.
	 */
	@Override
	public String toString() {
		return "GPS con ubicación en: " + this.punto.toString();
	}

}
