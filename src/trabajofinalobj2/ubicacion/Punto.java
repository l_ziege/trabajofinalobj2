package trabajofinalobj2.ubicacion;

/**
 * Representa un punto geografico - o coordenada - dentro del modelo desarrollado.
 */

public class Punto {

	private double x;
	private double y;
	
	/**
	 * Crea un punto
	 * @param x La posici�n x del punto.
	 * @param y La posici�n y del punto.
	 */
	public Punto(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Describe el componente X del punto.
	 * @return un double que representa la primer componente del punto.
	 */
	public double getX() {
		return this.x;
	}
	
	/**
	 * Describe el componente Y del punto.
	 * @return un double que representa la segunda componente del punto.
	 */
	public double getY() {
		return this.y;
	}
	
	/**
	 * Sobreescritura del metodo para representar mejor el punto.
	 */
	@Override
	public String toString() {
		return "Punto (" + this.x + ", " + this.y + ")";
	}
	
}
