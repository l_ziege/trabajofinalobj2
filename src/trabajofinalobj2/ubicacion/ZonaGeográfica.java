package trabajofinalobj2.ubicacion;

/**
 * Representa el area (fisica) que ocupa una zona de estacionamiento.
 */

public class ZonaGeográfica {

	private Punto origen;
	private double base;
	private double altura;
	
	/**
	 * Crea una zona geográfica.
	 * @param puntoOrigen El punto origen de la zona.
	 * @param base El ancho que ocupa la zona.
	 * @param altura El alto que ocupa la zona.
	 */
	public ZonaGeográfica(Punto puntoOrigen, double base, double altura) {
		this.origen = puntoOrigen;
		this.base = base;
		this.altura = altura;
	}
	
	/**
	 * Describe el limite (geografico) de la zona.
	 * @return un punto
	 */
	public Punto limiteGeográfico() {
		Punto limite = new Punto(this.origen.getX() + this.base, this.origen.getY() + this.altura);
		return limite;
	}

	/**
	 * Describe si el punto dado está incluido en la zona.
	 * @param punto
	 * @return un boolean, que indica si el punto pertenece a la zona
	 */
	public boolean incluyeElPunto(Punto punto) {
		return (this.contieneX(punto.getX())) && (this.contieneY(punto.getY()));
	}

	/**
	 * Describe si la Y recibida forma parte de la imagen de la zona
	 * @param y
	 * @return un booleano
	 */
	private boolean contieneY(double y) {
		return (this.origen.getY() <= y) && (this.limiteGeográfico().getY() >= y);
	}

	/**
	 * Describe si la X recibida forma parte del dominio de la zona
	 * @param x
	 * @return un booleano
	 */
	private boolean contieneX(double x) {
		return (this.origen.getX() <= x) && (this.limiteGeográfico().getX() >= x);
	}

	/**
	 * Describe la longitud de la zona
	 * @return un double que representa la longitud
	 */
	public double getLongitud() {
		return this.base;
	}
	
	/**
	 * Describe la altura de la zona
	 * @return un double que representa la altura
	 */
	public double getAltura() {
		return this.altura;
	}
	
	/**
	 * Sobreescritura del metodo para representar mejor la zona geográfica.
	 */
	@Override
	public String toString() {
		return "Zona Geografica entre " + this.origen.toString() + " y " + this.limiteGeográfico().toString();
	}
	
}
