package trabajofinalobj2.notificaciones;

/**
 * Busca representar las notificaciones dadas por la aplicación, cuando se busca empezar un estacionamiento,
 * pero no se tiene suficiente credito.
 * */

public class NotificaciónSaldoInsuficiente implements Notificación{

	/**
	 * Describe el texto mostrado en la notificación de la aplicación
	 * @return un string con el texto de la notificación
	 */
	@Override
	public String showNotification() {
		String s = "Saldo insuficiente para iniciar un estacionamiento";
		
		return s;
	}

}
