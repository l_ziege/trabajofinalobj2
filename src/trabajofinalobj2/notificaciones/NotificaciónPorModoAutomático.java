package trabajofinalobj2.notificaciones;

/**
 * Busca representar las notificaciones dadas por la aplicación, cuando el usuario quiere iniciar o finalizar
 * un estacionamiento desde la app estando en modo automático.
 * */

public class NotificaciónPorModoAutomático implements Notificación {
	
	/**
	 * Describe el texto mostrado en la notificación de la aplicación
	 * @return un string con el texto de la notificación
	 */
	@Override
	public String showNotification() {
		String s = "Los registros y finalizaciones de estacionamiento se hacen automáticamente en este modo.";
		
		return s;
	}

}
