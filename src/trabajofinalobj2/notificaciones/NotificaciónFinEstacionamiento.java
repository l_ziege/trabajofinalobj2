package trabajofinalobj2.notificaciones;

import trabajofinalobj2.estacionamientos.Estacionamiento;

/**
 * Busca representar las notificaciones dadas por la aplicación, cuando se termina exitosamente un estacionamiento
 * 
 * En este modelo, solo recibira estacionamientos via app (los que utilizan la app)
 * Sin embargo, se tipeo con Estacionamiento su atributo, con el fin de permitir recibir nuevos tipos de estacionamientos (por ejemplo, un nuevo
 * estacionamiento llamado estacionamientoReservado)
 * */

public class NotificaciónFinEstacionamiento implements Notificación {

	private Estacionamiento estAsociado;
	
	/**
	 * Crea una notificación de fin de estacionamiento.
	 * @param est El estacionamiento a notificar.
	 */
	public NotificaciónFinEstacionamiento(Estacionamiento est) {
		this.estAsociado = est;
	}
	
	/**
	 * Describe el texto mostrado en la notificación de la aplicación
	 * @return un string con el texto de la notificación
	 */
	@Override
	public String showNotification() {
		String s = "Estacionamiento finalizado." + "\n"
				+ "Vehiculo Asociado" + "\n"
				+ "Fecha: " + estAsociado.getFecha() + "\n"
				+ "Inicio: " + estAsociado.getHoraInicio() + "\n"
				+ "Fin: " + estAsociado.getHoraFin() + "\n"
				+ "Duración en horas: " + estAsociado.getHoras() + "\n"
				+ "Monto: " + this.estAsociado.costo();	
		
		return s;
	}

}
