package trabajofinalobj2.notificaciones;

/**
 * Posee el comportamiento que compartirán todos los tipos de notificaciones que se creen.
 * En un modelo mas amplio, todo objeto que busque mostrar en pantalla una notificacion deberia implementar esta interfaz.
 * */

public interface Notificación {
	
	/**
	 * Describe el texto mostrado en la notificación de la aplicación
	 * @return un string con el texto de la notificación
	 */
	public String showNotification();
	
}
