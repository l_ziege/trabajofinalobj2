package trabajofinalobj2.zonaDeEstacionamiento;

import java.util.ArrayList;
import java.util.List;

import trabajofinalobj2.ubicacion.Punto;
import trabajofinalobj2.ubicacion.ZonaGeográfica;

/**
 * Representa a la zona de estacionamiento en el modelo.
 */

public class ZonaDeEstacionamiento {

	private Inspector inspector;
	private List<PuntoDeVenta> puntosDeVenta;
	private ZonaGeográfica zonaGeográfica;

	/**
	 * Crea una zona de estacionamiento.
	 * @param inspector El inspector asignado a la zona.
	 * @param zonaGeografica El area fisica que ocupa la zona de estacionamiento.
	 */
	public ZonaDeEstacionamiento(Inspector inspector, ZonaGeográfica zonaGeografica) {
		this.inspector = inspector;
		this.puntosDeVenta = new ArrayList<PuntoDeVenta>();
		this.zonaGeográfica = zonaGeografica;
	}
	
	/**
	 * Registra un punto de venta dentro de la zona.
	 * @param pdv Un punto de venta a registrar.
	 */
	public void registrarPuntoDeVenta(PuntoDeVenta pdv) {
		this.puntosDeVenta.add(pdv);
	}
	
	/**
	 * Elimina el punto de venta de la zona.
	 * @param pdv Un punto de venta a eliminar.
	 */
	public void retirarPuntoDeVenta(PuntoDeVenta pdv) {
		this.puntosDeVenta.remove(pdv);
	}
	
	/**
	 * Retorna el punto de venta mas cercano al cliente.
	 * @return un punto de venta.
	 */
	public PuntoDeVenta puntoDeVentaMasCercano() {
		/*
		 * Aclaración: se asume que el punto de venta mas cercano siempre sera el primero de la zona
		 * Por motivos de tiempo (y prioridad de implementaciones dentro del modelo), se utilizo esta idea e implementacion
		 * 
		 * Una solucion mas acertada, seria agregar a los puntos de venta una instacia de Punto,
		 * y recorrer la lista de los mismos, quedandonos con aquel con la menor distancia entre su punto
		 * y la ubicación del usuario
		 */
		return this.puntosDeVenta.stream().findAny().get();
	}
	
	/**
	 * Describe al inspector asignado de la zona
	 * @return un inspector
	 */
	public Inspector getInspector() {
		return this.inspector;
	}
	
	/**
	 * Describe la lista de puntos de venta de la zona
	 * @return un arrayList de puntos de venta
	 */
	public List<PuntoDeVenta> getPuntos() {
		return this.puntosDeVenta;
	}
	
	/**
	 * Indica si el punto recibido esta en la zona
	 * @param punto El punto a verificar si esta dentro de la zona geografica de la zona de estacionamiento.
	 * @return un booleano que indica si el punto dado esta dentro de la zona de estacionamiento.
	 */
	public boolean estaEnZona(Punto punto) {
		return this.zonaGeográfica.incluyeElPunto(punto);
	}

}
