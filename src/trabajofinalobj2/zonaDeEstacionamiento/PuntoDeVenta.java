package trabajofinalobj2.zonaDeEstacionamiento;

/**
 * Representacion de los puntos de venta en el modelo.
 */

import java.time.LocalDateTime;

import trabajofinalobj2.compras.CompraPuntual;
import trabajofinalobj2.compras.CompraRecarga;
import trabajofinalobj2.estacionamientos.EstacionamientoPuntual;
import trabajofinalobj2.sem.SEM;
import trabajofinalobj2.usuario.Celular;
import trabajofinalobj2.usuario.Vehiculo;

public class PuntoDeVenta {
	
	private SEM sem;
	
	/**
	 * Crea un punto de venta.
	 * @param sem El sem del punto de venta.
	 */
	public PuntoDeVenta(SEM sem) {
		super();
		this.sem = sem;
	}
	
	/**
	 * Registra un estacionamiento puntual y su compra correspondiente de horas.
	 * @param vehiculo El vehiculo estacionado.
	 * @param fechaYHoraEstacionado La fecha y hora en la que se estacionó el vehiculo.
	 * @param cantHoras Las horas que se estacionará el vehiculo.
	 */
	public void registrarEstacionamientoYCompra(Vehiculo vehiculo, LocalDateTime fechaYHoraEstacionado, int cantHoras) {
		this.sem.registrarCompra(new CompraPuntual(this.sem.siguienteNúmeroDeCompra(), this, fechaYHoraEstacionado, cantHoras));
		this.sem.registrarEstacionamiento(new EstacionamientoPuntual(vehiculo, fechaYHoraEstacionado, cantHoras));
	}
	

	/**
	 * Registra la compra recarga de un celular y actualiza su credito si ya esta registrado, si no 
	 * lo registra con el monto dado.
	 * @param montoRecarga El monto a recargar.
	 * @param celular El celular a recargar credito.
	 * @param fechaYHoraDeRecarga La fecha y hora en la que se compro la recarga.
	 */
	public void registrarCompraRecarga(double montoRecarga, Celular celular, LocalDateTime fechaYHoraDeRecarga) {
		this.sem.registrarCompra(new CompraRecarga(this.sem.siguienteNúmeroDeCompra(), this, fechaYHoraDeRecarga, montoRecarga, celular));
		if (this.sem.estaRegistradoElCelular(celular)) {
			this.sem.actualizarCredito(celular, montoRecarga);
		} else {
			this.sem.registrarCelularConSuCredito(celular, montoRecarga);
		}
	}
	
}
