package trabajofinalobj2.usuario;

import trabajofinalobj2.app.AppUser;
import trabajofinalobj2.app.EstrategiaModoApp;
import trabajofinalobj2.ubicacion.GPS;
import trabajofinalobj2.ubicacion.Punto;

/**
 * Representa el celular por el cual los usuarios inician y finalizan estacionamientos.
 */

public class Celular {

	private String n�mero;
	private GPS gps;
	private AppUser app;
	
	/**
	 * Crea un celular con n�mero, gps y aplicaci�n.
	 * @param nro El n�mero del celular (representado con String).
	 * @param gps El GPS del celular.
	 * @param app La aplicaci�n del sem instalada en el celular.
	 */
	public Celular(String nro, GPS gps, AppUser app) {
		super();
		this.n�mero = nro;
		this.gps = gps;
		this.app = app;
	}
	
	/**
	 * Cambia el modo de aplicaci�n instalada en el celular.
	 * @param nuevoModo El modo de aplicaci�n nuevo a setear en la app.
	 */
	public void cambiarModoDeApp(EstrategiaModoApp nuevoModo) {
		this.app.cambiarModo(nuevoModo);
	}
	
	/**
	 * Consulta su saldo al SEM.
	 * @return El saldo del celular.
	 */
	public double consultarSaldo() {
		return this.app.consultarSaldoDeCelular(this);
	}
	
	/**
	 * Inicia sesi�n el usuario dado en la aplicaci�n del celular.
	 * @param usuario El usuario a iniciar sesi�n en la app del celular.
	 */
	public void iniciarSesi�nEnApp(Usuario usuario) {
		this.app.iniciarSesi�n(usuario);
	}
	
	/**
	 * Descibre la ubicaci�n del celular (modelada con un punto).
	 * @return Un punto que representa la ubicaci�n del celular.
	 */
	public Punto getUbicaci�n() {
		return this.gps.getPunto();
	}
	
	/**
	 * Retorna la aplicaci�n del SEM instalada en el celular.
	 * @return La AppUser instalada en el celular.
	 */
	public AppUser getApp() {
		return this.app;
	}
	
	/**
	 * Retorna el GPS del celular.
	 * @return El gps del celular.
	 */
	public GPS getGps() {
		return this.gps;
	}

	/**
	 * Retorna el n�mero del celular.
	 * @return El n�mero del celular (representado con un String).
	 */
	public String getN�mero() {
		return this.n�mero;
	}

}
