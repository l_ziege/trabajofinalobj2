package trabajofinalobj2.usuario;

/**
 * Representa el concepto de un vehiculo (para fines de este modelo, con poseer una patente es suficiente).
 * Al ser abstracta, en caso de querer a�adir otro tipo de vehiculo a futuro, unicamente se deberia extender 
 * de esta clase.
 */

public abstract class Vehiculo {

	protected String patente;
	
	/**
	 * Describe la patente del vehiculo.
	 * @return La patente.
	 */
	public String getPatente() {
		return this.patente;
	}
	
}
