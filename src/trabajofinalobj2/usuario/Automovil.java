package trabajofinalobj2.usuario;

/**
 * Representa a un automovil dentro del modelo desarrollado.
 * Se extiende de la clase abstracta Vehiculo, con el fin de permitir polimorfismo ante la aparición de 
 * futuras clases que representen vehiculos (como motocicletas u colectivos)
 */

public class Automovil extends Vehiculo {
	
	/**
	 * Crea un automovil
	 * @param patente La patente del automovil.
	 */
	public Automovil(String patente) {
		this.patente = patente;
	}
	
	/**
	 * Sobreescritura del metodo para representar mejor los automoviles.
	 */
	@Override
	public String toString() {
		return "Auto con patente: " + "[" + this.patente + "]";
	}

}
