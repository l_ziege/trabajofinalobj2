package trabajofinalobj2.usuario;

import trabajofinalobj2.app.EstrategiaModoApp;

/**
 * Representa los conductores del modelo.
 */

public class Usuario {

	private Vehiculo vehiculo;
	private Celular celular;
	
	/**
	 * Crea un usuario.
	 * @param vehiculo El vehiculo del usuario.
	 * @param celular El celular del usuario.
	 */
	public Usuario(Vehiculo vehiculo, Celular celular) {
		super();
		this.celular = celular;
		this.vehiculo = vehiculo;
		this.celular.iniciarSesiónEnApp(this);
	}
	
	/**
	 * Cambia el modo de la aplicación instalada en el celular del usuario.
	 * @param nuevoModo El modo de aplicación nuevo a setear en la app.
	 */
	public void cambiarModoDeApp(EstrategiaModoApp nuevoModo) {
		this.celular.cambiarModoDeApp(nuevoModo);
	}
	
	/**
	 * Descibre el saldo de su celular.
	 * @return El saldo del celular.
	 */
	public double consultarSaldo() {
		return this.celular.consultarSaldo();
	}
	
	/**
	 * Describe la patente del vehiculo del usuario.
	 * @return La patente del vehiculo del usuario.
	 */
	public String getPatenteDeVehiculo() {
		return this.vehiculo.getPatente();
	}
	
	/**
	 * Describe el vehiculo del usuario.
	 * @return El vehiculo.
	 */
	public Vehiculo getVehiculo() {
		return this.vehiculo;
	}
	
	/**
	 * Describe el celular del usuario.
	 * @return El celular.
	 */
	public Celular getCelular() {
		return this.celular;
	}
	
}
