package trabajofinalobj2.app;

import java.time.LocalDateTime;

import trabajofinalobj2.notificaciones.Notificación;
import trabajofinalobj2.usuario.Celular;
import trabajofinalobj2.usuario.Vehiculo;

/**
 * Representa los modos que puede tener la aplicación, actualmente son Modo Automático y Modo Manual.
 */

public interface EstrategiaModoApp {

	/**
	 * Indica a la aplicación que hacer al recibir el mensaje driving según su modo actual.
	 * @param app La app a la cual enviarle mensajes segun el modo.
	 */
	public void driving(AppUser app);

	/**
	 * Indica a la aplicación que hacer al recibir el mensaje walking según su modo actual.
	 * @param app La app a la cual enviarle mensajes segun el modo.
	 */
	public void walking(AppUser app);

	/**
	 * Describe una notificación sobre la realización del registro del estacionamiento según su modo actual.
	 * @param vehiculoEstacionado El vehiculo que se quiere estacionar.
	 * @param fechaYHoraEstacionado La fecha y la hora en la que se estacionó el vehiculo.
	 * @param celular El celular con el que cual se registra el estacionamiento.
	 * @param app La app a la cual enviarle mensajes segun el modo.
	 * @return Una notificación que dependerá del modo actual de la aplicación y del saldo de celular que registra el estacionamiento.
	 */
	public Notificación realizarRegistroDeEstacionamiento(Vehiculo vehiculoEstacionado, LocalDateTime fechaYHoraEstacionado, Celular celular, AppUser app);

	/**
	 * Describe una notificación sobre la realización de la finalización del estacionamiento según su modo actual.
	 * @param celular El celular que realizó anteriormente el estacionamiento.
	 * @param fechaYHoraFinEstacionamiento La fecha y la hora en la que se desea finalizar el estacionamiento.
	 * @param app La app a la cual enviarle mensajes segun el modo.
	 * @return Una notificación que dependerá del modo actual de la aplicación.
	 */
	public Notificación realizarFinalizaciónDeEstacionamiento(Celular celular, LocalDateTime fechaYHoraFinEstacionamiento, AppUser app);
	
	/**
	 * Indica si hay un estacionamiento vigente del usuario registrado actualmente en la app.
	 * @param app La app donde verificar si hay un estacionamiento vigente.
	 * @return Un booleano, verdadero si hay un estacionamiento vigente del usuario, falso si no.
	 */
	public boolean hayEstacionamientoVigenteDelUsuarioRegistrado(AppUser app);
	
	/**
	 * Indica si el celular del usuario registrado se encuentra dentro de una zona de estacionamiento del sem.
	 * @param app La app donde verificar si se encuentra el celular del usuario registrado en una zona de estacionamiento.
	 * @return Un booleano, verdadero si el celular del usuario se encuentra dentro de una zona de estacionamiento del sem, falso si no.
	 */
	public boolean seEncuentraElCelularDelUsuarioRegistradoDentroDeUnaZonaDeEstacionamiento(AppUser app);
	
}
