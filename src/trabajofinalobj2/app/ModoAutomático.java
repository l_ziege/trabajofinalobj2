package trabajofinalobj2.app;

import java.time.LocalDateTime;

import trabajofinalobj2.estacionamientos.Estacionamiento;
import trabajofinalobj2.notificaciones.Notificación;
import trabajofinalobj2.notificaciones.NotificaciónFinEstacionamiento;
import trabajofinalobj2.notificaciones.NotificaciónInicioEstacionamiento;
import trabajofinalobj2.notificaciones.NotificaciónPorModoAutomático;
import trabajofinalobj2.usuario.Celular;
import trabajofinalobj2.usuario.Vehiculo;

/**
 * Representa el modo automático de las aplicaciones.
 */

public class ModoAutomático implements EstrategiaModoApp {

	/**
	 * Finaliza un estacionamiento si hay uno vigente del usuario registrado en la app.
	 * @param app La app donde finalizar un estacionamiento si debe.
	 */
	@Override
	public void driving(AppUser app) {
		// El usuario tiene un solo vehiculo, si tiene un estacionamiento vigente, seguro el cambio de 
		// movimiento se realizo en la misma ubicación donde se inicio dicho estacionamiento.
		if (this.hayEstacionamientoVigenteDelUsuarioRegistrado(app)) {
			this.finalizarEstacionamiento(app);
		}
	}

	/**
	 * Finaliza el estacionamiento actual e informa dicha acción.
	 * @param app La app donde finalizar un estacionamiento.
	 */
	private void finalizarEstacionamiento(AppUser app) {
		app.finalizarEstacionamientoEnModoAutomático();
		this.informarFinalizaciónDeEstacionamiento(app);
	}

	/**
	 * Informa el estacionamiento finalizado.
	 * @param app La app a la cual pedirle el ultimo estacionamiento finalizado del sem.
	 */
	private void informarFinalizaciónDeEstacionamiento(AppUser app) {
		Estacionamiento estacionamientoFinalizado = app.pedirÚltimoEstacionamientoFinalizadoAlSEM();
		NotificaciónFinEstacionamiento notificaciónDeFin = new NotificaciónFinEstacionamiento(estacionamientoFinalizado);
		System.out.println(notificaciónDeFin.showNotification());
	}

	/**
	 * Inicia un estacionamiento si no hay uno vigente del usuario registrado en la app y si el celular
	 * del usuario se encuentra dentro de una zona de estacionamiento.
	 * @param app La app donde iniciar un estacionamiento si debe.
	 */
	@Override
	public void walking(AppUser app) {
		if (!this.hayEstacionamientoVigenteDelUsuarioRegistrado(app) && this.seEncuentraElCelularDelUsuarioRegistradoDentroDeUnaZonaDeEstacionamiento(app)) {
			this.iniciarEstacionamiento(app);
		}
	}

	/**
	 * Inicia un estacionamiento e informa dicha acción.
	 * @param app La app donde iniciar un estacionamiento.
	 */
	private void iniciarEstacionamiento(AppUser app) {
		app.iniciarEstacionamientoEnModoAutomático();
		this.informarInicioDeEstacionamiento(app);
	}

	/**
	 * Informa el estacionamiento iniciado.
	 * @param app La app a la cual pedirle el ultimo estacionamiento registrado del sem.
	 */
	private void informarInicioDeEstacionamiento(AppUser app) {
		Estacionamiento estacionamientoIniciado = app.pedirÚltimoEstacionamientoRegistradoAlSEM();
		NotificaciónInicioEstacionamiento notificaciónDeInicio = new NotificaciónInicioEstacionamiento(estacionamientoIniciado);
		System.out.println(notificaciónDeInicio.showNotification());;
	}

	/**
	 * Describe una notificación de modo automático.
	 * @param vehiculoEstacionado El vehiculo que se quiere estacionar.
	 * @param fechaYHoraEstacionado La fecha y la hora en la que se estacionó el vehiculo.
	 * @param celular El celular con el que cual se registra el estacionamiento.
	 * @param app La app donde iniciar un estacionamiento si es posible.
	 * @return Una notificación por modo automático.
	 */
	@Override
	public Notificación realizarRegistroDeEstacionamiento(Vehiculo vehiculoEstacionado, LocalDateTime fechaYHoraEstacionado, Celular celular, AppUser app) {
		return new NotificaciónPorModoAutomático();
		// En el modo automático el usuario no puede registrar estacionamientos, lo hace la app por si misma.
	}

	/**
	 * Describe una notificación de modo automático.
	 * @param celular El celular que realizó anteriormente el estacionamiento.
	 * @param fechaYHoraFinEstacionamiento La fecha y la hora en la que se desea finalizar el estacionamiento.
	 * @param app La app donde finalizar un estacionamiento.
	 * @return Una notificación por modo automático.
	 */
	@Override
	public Notificación realizarFinalizaciónDeEstacionamiento(Celular celular, LocalDateTime fechaYHoraFinEstacionamiento, AppUser app) {
		return new NotificaciónPorModoAutomático();
		// En el modo automático el usuario no puede finalizar estacionamientos, lo hace la app por si misma.
	}

	/**
	 * Indica si hay un estacionamiento vigente del usuario registrado actualmente en la app.
	 * @param app La app donde verificar si hay un estacionamiento vigente.
	 * @return Un booleano, verdadero si hay un estacionamiento vigente del usuario, falso si no.
	 */
	@Override
	public boolean hayEstacionamientoVigenteDelUsuarioRegistrado(AppUser app) {
		return app.hayEstacionamientoVigenteDelUsuarioRegistrado();
	}

	/**
	 * Indica si el celular del usuario registrado se encuentra dentro de una zona de estacionamiento del sem.
	 * @param app La app donde verificar si se encuentra el celular del usuario registrado en una zona de estacionamiento.
	 * @return Un booleano, verdadero si el celular del usuario se encuentra dentro de una zona de estacionamiento del sem, falso si no.
	 */
	@Override
	public boolean seEncuentraElCelularDelUsuarioRegistradoDentroDeUnaZonaDeEstacionamiento(AppUser app) {
		return app.seEncuentraElCelularDelUsuarioRegistradoDentroDeUnaZonaDeEstacionamiento();
	}
	
	/**
	 * Sobreescritura del metodo para representar mejor el Modo Automático de las aplicaciones.
	 */
	@Override
	public String toString() {
		return "Modo de app Automático.";
	}

}
