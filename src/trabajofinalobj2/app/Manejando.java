package trabajofinalobj2.app;

public class Manejando extends EstadoMovimiento {

	/**
	 * Indica a la aplicación dada por parámetro que hubo un cambio de movimiento.
	 * @param app La aplicación a indicarle que hacer.
	 */
	@Override
	public void walking(AppUser app) {
		app.indicarCambioDeMovimientoWalkingAlModo();
		app.cambiarEstadoMov(new Caminando());
	}

	/**
	 * Metodo hook, la app ya estaba caminando.
	 */
	@Override
	public void driving(AppUser app) {
		// No hace nada, ya estaba manejando.
	}
	
	/**
	 * Sobreescritura del metodo para representar mejor el movimiento Caminando.
	 */
	@Override
	public String toString() {
		return "Estado movimiento manejando.";
	}


}
