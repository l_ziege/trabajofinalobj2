package trabajofinalobj2.app;

import java.time.LocalDateTime;

import trabajofinalobj2.estacionamientos.Estacionamiento;
import trabajofinalobj2.estacionamientos.EstacionamientoViaApp;
import trabajofinalobj2.notificaciones.Notificación;
import trabajofinalobj2.sem.SEM;
import trabajofinalobj2.usuario.Celular;
import trabajofinalobj2.usuario.Usuario;
import trabajofinalobj2.usuario.Vehiculo;

/**
 * Representa la aplicación instalada en los celulares de los usuarios provista por el SEM.
 */

public class AppUser implements MovementSensor {
	
	private SEM sem;
	private Usuario usuarioRegistrado;
	private EstrategiaModoApp modo;
	private EstadoMovimiento estadoMov;
	
	/**
	 * Crea una app de celular en modo manual.
	 * @param sem El SEM por el cual fue creada la app.
	 * @para mov El Movimiento inicial de la app instalada en un celular.
	 */
	public AppUser(SEM sem, EstrategiaModoApp modo, EstadoMovimiento mov) {
		super();
		this.sem = sem;
		this.modo = modo;
		this.estadoMov = mov;
	}

	/**
	 * Delega a su estado movimiento el mensaje para que responda de la forma adecuada.
	 */
	@Override
	public void driving() {
		this.estadoMov.driving(this);
	}
	
	/**
	 * Delega a su estado modo el mensaje driving para que responda de la forma adecuada.
	 */
	public void indicarCambioDeMovimientoDrivingAlModo() {
		this.modo.driving(this);
	}
	
	/**
	 * Finaliza un estacionamiento via app en modo automático.
	 */
	public void finalizarEstacionamientoEnModoAutomático() {
		this.sem.finalizarEstacionamientoViaApp(this.getCelularDelUsuario(), LocalDateTime.now());
	}
	
	/**
	 * Describe el último estacionamiento finalizado del sem.
	 * @return El último estacionamiento finalizado en el sem.
	 */
	public Estacionamiento pedirÚltimoEstacionamientoFinalizadoAlSEM() {
		return this.sem.getÚltimoEstacionamientoFinalizado();
	}

	/**
	 * Delega a su estado movimiento el mensaje para que responda de la forma adecuada.
	 */
	@Override
	public void walking() {
		this.estadoMov.walking(this);
	}
	
	/**
	 * Delega a su estado modo el mensaje walking para que responda de la forma adecuada.
	 */
	public void indicarCambioDeMovimientoWalkingAlModo() {
		this.modo.walking(this);
	}
	
	/**
	 * Registra un estacionamiento via app en modo automático.
	 */
	public void iniciarEstacionamientoEnModoAutomático() {
		EstacionamientoViaApp estacionamientoAIniciar = new EstacionamientoViaApp(this.getVehiculoDelUsuario(), LocalDateTime.now(), this.getCelularDelUsuario());
		this.sem.registrarEstacionamiento(estacionamientoAIniciar);
	}
	
	/**
	 * Describe el último estacionamiento iniciado del sem.
	 * @return El último estacionamiento registrado en el sem.
	 */
	public Estacionamiento pedirÚltimoEstacionamientoRegistradoAlSEM() {
		return this.sem.getÚltimoEstacionamientoRegistrado();
	}

	/**
	 * Describe el saldo del celular dado por parámetro registrado en el sem.
	 * @param celular El celular del cual se quiere saber su saldo.
	 * @return El saldo del celular consultado.
	 */
	public double consultarSaldoDeCelular(Celular celular) {
		return this.sem.getCreditoDeCelular(celular);
	}

	/**
	 * Delega a su estado modo el mensaje para que responda de la forma adecuada.
	 * @param vehiculoEstacionado El vehiculo que se quiere estacionar.
	 * @param fechaYHoraEstacionado La fecha y la hora en la que se estacionó el vehiculo.
	 * @param celular El celular con el que cual se registra el estacionamiento.
	 * @return Una notificación que dependerá del modo actual de la aplicación y del saldo de celular que registra el estacionamiento.
	 */
	public Notificación realizarRegistroDeEstacionamiento(Vehiculo vehiculoEstacionado, LocalDateTime fechaYHoraEstacionado, Celular celular) {
		return this.modo.realizarRegistroDeEstacionamiento(vehiculoEstacionado, fechaYHoraEstacionado, celular, this);
	}
	
	/**
	 * Registra un estacionamiento via app en modo manual.
	 * @param estacionamientoAIniciar El estacionamiento a registrar.
	 */
	public void iniciarEstacionamientoEnModoManual(Estacionamiento estacionamientoAIniciar) {
		this.sem.registrarEstacionamiento(estacionamientoAIniciar);
	}

	/**
	 * Delega a su estado modo el mensaje para que responda de la forma adecuada.
	 * @param celular El celular que realizó anteriormente el estacionamiento.
	 * @param fechaYHoraFinEstacionamiento La fecha y la hora en la que se desea finalizar el estacionamiento.
	 * @return Una notificación que dependerá del modo actual de la aplicación.
	 */
	public Notificación realizarFinalizaciónDeEstacionamiento(Celular celular, LocalDateTime fechaYHoraFinEstacionamiento) {
		return this.modo.realizarFinalizaciónDeEstacionamiento(celular, fechaYHoraFinEstacionamiento, this);
	}
	
	/**
	 * Finaliza un estacionamiento via app en modo manual.
	 * @param fechaYHoraFinEstacionamiento La fecha y la hora en la que se desea finalizar el estacionamiento.
	 */
	public void finalizarEstacionamientoEnModoManual(LocalDateTime fechaYHoraFinEstacionamiento) {
		this.sem.finalizarEstacionamientoViaApp(getCelularDelUsuario(), fechaYHoraFinEstacionamiento);
	}
	
	/**
	 * Cambia el modo actual de la aplicación.
	 * @param nuevoModo El nuevo modo seteado por el usuario.
	 */
	public void cambiarModo(EstrategiaModoApp nuevoModo) {
		this.modo = nuevoModo;
	}
	
	/**
	 * Cambia el movimiento actual de la aplicación
	 * @param nuevoEstadoMov El movimiento que tiene ahora la aplicación.
	 */
	public void cambiarEstadoMov(EstadoMovimiento nuevoEstadoMov) {
		this.estadoMov = nuevoEstadoMov;
	}
	
	/**
	 * Inicia sesión en la aplicación el usuario dado.
	 * @param usuario El usuario que desea iniciar sesión.
	 */
	public void iniciarSesión(Usuario usuario) {
		this.usuarioRegistrado = usuario;
	}
	
	/**
	 * Indica si hay un estacionamiento vigente de la patente del usuario registrado actualmente en la app.
	 * @return Un booleano, verdadero si hay un estacionamiento vigente de la patente del usuario, falso si no.
	 */
	public boolean hayEstacionamientoVigenteDelUsuarioRegistrado() {
		return this.sem.existeEstacionamientoConPatente(this.getPatenteDelVehiculoDelUsuario());
	}
	
	/**
	 * Describe la patente del vehiculo del usuario.
	 * @return La patente del vehiculo del usuario.
	 */
	private String getPatenteDelVehiculoDelUsuario() {
		return this.usuarioRegistrado.getPatenteDeVehiculo();
	}
	
	/**
	 * Indica si el celular del usuario registrado se encuentra dentro de una zona de estacionamiento del sem.
	 * @return Un booleano, verdadero si el celular del usuario se encuentra dentro de una zona de estacionamiento del sem, falso si no.
	 */
	public boolean seEncuentraElCelularDelUsuarioRegistradoDentroDeUnaZonaDeEstacionamiento() {
		return this.sem.laUbicaciónDelCelularSeEncuentraDentroDeUnaZonaDeEstacionamiento(this.getCelularDelUsuario());
	}
	
	/**
	 * Describe el vehiculo del usuario registrado en la app.
	 * @return El vehiculo del usuario.
	 */
	private Vehiculo getVehiculoDelUsuario() {
		return this.usuarioRegistrado.getVehiculo();
	}
	
	/**
	 * Describe el celular del usuario registrado en la app.
	 * @return El celular del usuario.
	 */
	private Celular getCelularDelUsuario() {
		return this.usuarioRegistrado.getCelular();
	}
	
	/**
	 * Indica si está registrado el celular del usuario en el sem.
	 * @return Un booleano, verdadero si está registrado el celular del usuario en el sem, falso si no.
	 */
	public boolean estaRegistradoElCelularDelUsuarioEnElSEM() {
		return this.sem.estaRegistradoElCelular(this.getCelularDelUsuario());
	}
	
	/**
	 * Describe el usuario registrado en la aplicación.
	 * @return El usuario que está actualmente en la aplicación.
	 */
	public Usuario getUsuarioRegistrado() {
		return this.usuarioRegistrado;
	}

	/**
	 * Describe el SEM que creó la aplicación y al cual está asociado.
	 * @return El SEM creador de la aplicación.
	 */
	public SEM getSem() {
		return this.sem;
	}
	
	/**
	 * Describe el modo actual de la aplicación.
	 * @return El modo actual de la aplicación, puede ser modo manual o modo automático.
	 */
	public EstrategiaModoApp getModo() {
		return this.modo;
	}
	
	/**
	 * Describe el movimiento actual de la aplicación.
	 * @return El movimiento actual de la aplicación, puede ser caminando o manejando.
	 */
	public EstadoMovimiento getEstadoMov() {
		return this.estadoMov;
	}

}
