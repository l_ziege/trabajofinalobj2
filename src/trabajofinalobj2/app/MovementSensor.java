package trabajofinalobj2.app;

/**
 * Interfaz que representa el sensor que informa de los cambios de movimiento de las aplicaciones.
 */

public interface MovementSensor {

	/**
	 * Informa el movimiento driving a las clases que implementen esta interfaz.
	 */
	public void driving();
	
	/**
	 * Informa el movimiento walking a las clases que implementen esta interfaz.
	 */
	public void walking();
	
}
