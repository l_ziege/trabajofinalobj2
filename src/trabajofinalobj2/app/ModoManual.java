package trabajofinalobj2.app;

import java.time.LocalDateTime;

import trabajofinalobj2.estacionamientos.Estacionamiento;
import trabajofinalobj2.estacionamientos.EstacionamientoViaApp;
import trabajofinalobj2.notificaciones.Notificación;
import trabajofinalobj2.notificaciones.NotificaciónFinEstacionamiento;
import trabajofinalobj2.notificaciones.NotificaciónInicioEstacionamiento;
import trabajofinalobj2.notificaciones.NotificaciónSaldoInsuficiente;
import trabajofinalobj2.sem.SEM;
import trabajofinalobj2.usuario.Celular;
import trabajofinalobj2.usuario.Vehiculo;

/**
 * Representa el modo manual de las aplicaciones.
 */

public class ModoManual implements EstrategiaModoApp {

	/**
	 * Notifca una posible finalización de estacionamiento si hay uno vigente del usuario registrado en la app.
	 * @param app La app a verificar si hay un estacionamiento vigente de su usuario registrado.
	 */
	@Override
	public void driving(AppUser app) {
		// El usuario tiene un solo vehiculo, si tiene un estacionamiento vigente, seguro el cambio de 
		// movimiento se realizo en la misma ubicación donde se inicio dicho estacionamiento.
		if (this.hayEstacionamientoVigenteDelUsuarioRegistrado(app)) {
			this.notificarPosibleFin();
		}
	}

	/**
	 * Notifica una posible finalización de estacionamiento.
	 */
	private void notificarPosibleFin() {
		System.out.println("Posible finalización de estacionamiento pendiente.");
	}

	/**
	 * Notifica un posible inicio de estacionamiento si no hay uno vigente del usuario registrado en la app 
	 * y si el celular del usuario se encuentra dentro de una zona de estacionamiento.
	 * @param app La app a verificar si tiene un estacionamiento vigente su usuario registrado y si el celular del mismo se encuentra en una zona.
	 */
	@Override
	public void walking(AppUser app) {
		if (!this.hayEstacionamientoVigenteDelUsuarioRegistrado(app) && this.seEncuentraElCelularDelUsuarioRegistradoDentroDeUnaZonaDeEstacionamiento(app)) {
			this.notificarPosibleInicio();
		}
	}

	/**
	 * Notifica un posible inicio de estacionamiento.
	 */
	private void notificarPosibleInicio() {
		System.out.println("Posible inicio de estacionamiento pendiente.");
	}

	/**
	 * Describe una notificación de inicio de estacionamiento si el celular del usuario registrado tiene
	 * saldo suficiente, si no una notificación de saldo insuficiente..
	 * @param vehiculoEstacionado El vehiculo que se quiere estacionar.
	 * @param fechaYHoraEstacionado La fecha y la hora en la que se estacionó el vehiculo.
	 * @param celular El celular con el que cual se registra el estacionamiento.
	 * @param app La app donde iniciar un estacionamiento si es posible.
	 * @return Una notificación de inicio de estacionamiento si el celular del usuario tiene saldo suficiente, si no una de saldo insuficiente.
	 */
	@Override
	public Notificación realizarRegistroDeEstacionamiento(Vehiculo vehiculoEstacionado, LocalDateTime fechaYHoraEstacionado, Celular celular, AppUser app) {
		if (app.estaRegistradoElCelularDelUsuarioEnElSEM() && this.tieneSaldoSuficienteParaPagarUnEstacionamiento(celular, app)) {
			Estacionamiento estacionamientoARegistrar = new EstacionamientoViaApp(vehiculoEstacionado, fechaYHoraEstacionado, celular);
			app.iniciarEstacionamientoEnModoManual(estacionamientoARegistrar);
			return new NotificaciónInicioEstacionamiento(estacionamientoARegistrar);
		} else {
			return new NotificaciónSaldoInsuficiente();
		}
	}
	
	/**
	 * Indica si el celular dado por parámetro tiene saldo suficiente para pagar un estacionamiento.
	 * @param celular El celular a verificar si tiene saldo suficiente.
	 * @param app La app donde consultar el saldo del celular.
	 * @return Un booleano, verdadero si el celular dado tiene saldo suficiente, falso si no.
	 */
	private boolean tieneSaldoSuficienteParaPagarUnEstacionamiento(Celular celular, AppUser app) {
		return (app.consultarSaldoDeCelular(celular) >= new SEM().precioPorHora());
	}

	/**
	 * Describe una notificación sobre la realización de la finalización del estacionamiento.
	 * @param celular El celular que realizó anteriormente el estacionamiento.
	 * @param fechaYHoraFinEstacionamiento La fecha y la hora en la que se desea finalizar el estacionamiento.
	 * @param app La app donde finalizar un estacionamiento.
	 * @return Una notificación de finalización de estacionamiento.
	 */
	@Override
	public Notificación realizarFinalizaciónDeEstacionamiento(Celular celular, LocalDateTime fechaYHoraFinEstacionamiento, AppUser app) {
		app.finalizarEstacionamientoEnModoManual(fechaYHoraFinEstacionamiento);
		Estacionamiento estacionamientoFinalizado = app.pedirÚltimoEstacionamientoFinalizadoAlSEM();
		return new NotificaciónFinEstacionamiento(estacionamientoFinalizado);
	}

	/**
	 * Indica si hay un estacionamiento vigente del usuario registrado actualmente en la app.
	 * @param app La app donde verificar si hay un estacionamiento vigente.
	 * @return Un booleano, verdadero si hay un estacionamiento vigente del usuario, falso si no.
	 */
	@Override
	public boolean hayEstacionamientoVigenteDelUsuarioRegistrado(AppUser app) {
		return app.hayEstacionamientoVigenteDelUsuarioRegistrado();
	}

	/**
	 * Indica si el celular del usuario registrado se encuentra dentro de una zona de estacionamiento del sem.
	 * @param app La app donde verificar si se encuentra el celular del usuario registrado en una zona de estacionamiento.
	 * @return Un booleano, verdadero si el celular del usuario se encuentra dentro de una zona de estacionamiento del sem, falso si no.
	 */
	@Override
	public boolean seEncuentraElCelularDelUsuarioRegistradoDentroDeUnaZonaDeEstacionamiento(AppUser app) {
		return app.seEncuentraElCelularDelUsuarioRegistradoDentroDeUnaZonaDeEstacionamiento();
	}

	/**
	 * Sobreescritura del metodo para representar mejor el Modo Manual de las aplicaciones.
	 */
	@Override
	public String toString() {
		return "Modo de app Manual.";
	}
	
}
