package trabajofinalobj2.app;

/**
 * Representa el movimiento caminando de las aplicaciones instaladas en los celulares de los usuarios. 
 */

public class Caminando extends EstadoMovimiento {

	/**
	 * Metodo hook, la app ya estaba caminando.
	 */
	@Override
	public void walking(AppUser app) {
		// No hace nada, ya estaba caminando.
	}

	/**
	 * Indica a la aplicación dada por parámetro que hubo un cambio de movimiento.
	 * @param app La aplicación a indicarle que hacer.
	 */
	@Override
	public void driving(AppUser app) {
		app.indicarCambioDeMovimientoDrivingAlModo();
		app.cambiarEstadoMov(new Manejando());
	}
	
	/**
	 * Sobreescritura del metodo para representar mejor el movimiento Caminando.
	 */
	@Override
	public String toString() {
		return "Estado movimiento caminando.";
	}

}
