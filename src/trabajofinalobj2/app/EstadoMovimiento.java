package trabajofinalobj2.app;

/**
 * Representa los distintos movimientos que tienen las aplicaciones instaladas en los celulares de los usuarios.
 * Actualmente solo hay 2 movimientos, Caminando y Manejando.
 */

public abstract class EstadoMovimiento {

	/**
	 * Indica a la aplicaci�n dada que hacer al recibir el mensaje walking seg�n su movimiento actual.
	 * @param app La aplicaci�n a indicarle que hacer.
	 */
	public abstract void walking(AppUser app);
	
	/**
	 * Indica a la aplicaci�n dada que hacer al recibir el mensaje driving seg�n su movimiento actual.
	 * @param app La aplicaci�n a indicarle que hacer.
	 */
	public abstract void driving(AppUser app);
}
