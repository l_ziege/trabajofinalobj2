package trabajofinalobj2.sem;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import trabajofinalobj2.usuario.Vehiculo;
import trabajofinalobj2.zonaDeEstacionamiento.Inspector;
import trabajofinalobj2.zonaDeEstacionamiento.ZonaDeEstacionamiento;

/**
 * Representación de las infracciones dentro del modelo 
 */

public class Infracción {
	
	private LocalDateTime fechaDeEmisión;
	private Vehiculo vehiculo;
	private Inspector inspector;
	private ZonaDeEstacionamiento zona;
	
	/**
	 * Crea una infracción.
	 * @param vehiculo El vehiculo al que se realizó la infracción.
	 * @param inspector El inspector que realizó la infracción.
	 * @param zona La zona donde se realizó la infracción.
	 * @param emision La fecha y hora en la que se realizó la infracción.
	 */
	public Infracción(Vehiculo vehiculo, Inspector inspector, ZonaDeEstacionamiento zona, LocalDateTime emision) {
		this.fechaDeEmisión = emision;
		this.vehiculo = vehiculo;
		this.zona = zona;
		this.inspector = inspector;
	}

	/**
	 * Describe la fecha en la que se labro la infracción (d/m/a)
	 * @return un string con la fecha de alta de la infracción
	 */
	public String getFecha() {
		DateTimeFormatter formatoFechas = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		return fechaDeEmisión.format(formatoFechas);
	}
	
	/**
	 * Describe la hora en la que se labro el alta de infracción (h:m)
	 * @return un string con la hora en la que se levantó la infracción
	 */
	public String getHora() {
		DateTimeFormatter formatoHoras = DateTimeFormatter.ofPattern("HH:mm");
		return fechaDeEmisión.format(formatoHoras);
	}

	/**
	 * Describe el vehiculo multado
	 * @return un vehiculo
	 */
	public Vehiculo getVehiculo() {
		return this.vehiculo;
	}

	/**
	 * Describe el inspector que levantó la infracción
	 * @return un inspector
	 */
	public Inspector getInspector() {
		return this.inspector;
	}

	/**
	 * Describe la zona en la que se registró la infracción
	 * @return una zona de estacionamiento
	 */
	public ZonaDeEstacionamiento getZona() {
		return this.zona;
	}
	
	/**
	 * Sobreescritura del metodo para representar mejor una infracción.
	 */
	@Override
	public String toString() {
		return "# INFRACCION" + "\n"
				+ "Vehiculo multado: " + this.vehiculo + "\n"
				+ "Emitida el: " + this.getFecha() + " a las: " + this.getHora();
	}
	
}
