package trabajofinalobj2.sem;

/**
 * Representacion abstracta de las entidades que se suscriben al SEM
 * Implementan la interfaz SEMlistener
 */

public abstract class Entidad implements SEMListener {

	private SEM semObservado;
	
	/**
	 * Registra el sem a observar.
	 * @param observable El sem a observar.
	 */
	public void registrarObservable(SEM observable) {
		this.semObservado = observable;
	}
	
	/**
	 * Describe el sem que se est� observando
	 * @return El sem que se observa.
	 */
	public SEM getSEM() {
		return this.semObservado;
	}
	
}
