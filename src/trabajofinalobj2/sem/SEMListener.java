package trabajofinalobj2.sem;

import trabajofinalobj2.compras.Compra;
import trabajofinalobj2.estacionamientos.Estacionamiento;

/**
 * Interfaz creada con la finalidad de brindar funcionalidad a los observadores del SEM, en base al patron 
 * de dise�o observer.
 * Las clases que implementen esta interfaz agregar�n comportamiento a los metodos que reciben notificaciones
 * y har�n lo que necesiten con cada notificaci�n.
 */

public interface SEMListener {

	/**
	 * Recibe una notificaci�n del sem observado dado por par�metro sobre un nuevo estacionamiento registrado.
	 * @param sem El sem que notifica.
	 * @param nuevoEstacionamiento El nuevo estacionamiento registrado.
	 */
	public void nuevoEstacionamientoRegistrado(SEM sem, Estacionamiento nuevoEstacionamiento);
	
	/**
	 * Recibe una notificaci�n del sem observado dado por par�metro sobre una nueva compra.
	 * @param sem El sem que notitifica.
	 * @param nuevaCompra La nueva compra.
	 */
	public void nuevaCompraRegistrada(SEM sem, Compra nuevaCompra);
	
	/**
	 * Recibe una notificaci�n del sem observado dado por par�metro sobre un estacionamiento finalizado.
	 * @param sem El sem que notifica.
	 * @param estacionamiento El estacionamiento finalizado.
	 */
	public void estacionamientoFinalizado(SEM sem, Estacionamiento estacionamiento);
	
	/**
	 * Registra el sem a observar.
	 * @param observable El sem a observar.
	 */
	public void registrarObservable(SEM observable);
	
}
