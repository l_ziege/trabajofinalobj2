package trabajofinalobj2.sem;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import trabajofinalobj2.compras.Compra;
import trabajofinalobj2.estacionamientos.Estacionamiento;
import trabajofinalobj2.estacionamientos.EstacionamientoPuntual;
import trabajofinalobj2.estacionamientos.EstacionamientoViaApp;
import trabajofinalobj2.usuario.Celular;
import trabajofinalobj2.usuario.Vehiculo;
import trabajofinalobj2.zonaDeEstacionamiento.ZonaDeEstacionamiento;

/**
 * Representa el sistema de estacionamiento medido del modelo.
 */

public class SEM {

	private List<Compra> compras;
	private List<ZonaDeEstacionamiento> zonasDeEstacionamiento;
	private List<CreditoPorCelular> creditosPorCelular;
	private List<Estacionamiento> estacionamientosVigentes;
	private List<Estacionamiento> estacionamientosHistoricos;
	private List<Infracci�n> infracciones;
	private List<SEMListener> observadores;
	
	/**
	 * Crea un sem.
	 */
	public SEM() {
		super();
		this.compras = new ArrayList<Compra>();
		this.zonasDeEstacionamiento = new ArrayList<ZonaDeEstacionamiento>();
		this.creditosPorCelular = new ArrayList<CreditoPorCelular>();
		this.estacionamientosVigentes = new ArrayList<Estacionamiento>();
		this.estacionamientosHistoricos = new ArrayList<Estacionamiento>();
		this.infracciones = new ArrayList<Infracci�n>();
		this.observadores = new ArrayList<SEMListener>();
	}
	
	/**
	 * Registra la zona de estacionamiento dada por par�metro si a�n no se registr�.
	 * @param zona La zona de estacionamiento a registrar.
	 */
	public void registrarZonaDeEstacionamiento(ZonaDeEstacionamiento zona) {
		if (!this.zonasDeEstacionamiento.contains(zona)) {
			this.zonasDeEstacionamiento.add(zona);
		}
	}
	
	/**
	 * Registra la compra dada por par�metro si a�n no se registr�.
	 * @param compra La compra a registrar.
	 */
	public void registrarCompra(Compra compra) {
		if (!this.compras.contains(compra)) {
			this.compras.add(compra);
			this.notificarSiEsCompraRecarga(compra);
		}
	}
	
	/**
	 * Notifica a los observadores la compra si la misma es una recarga.
	 * @param compra La compra a notificar si es recarga.
	 */
	private void notificarSiEsCompraRecarga(Compra compra) {
		if (compra.esCompraRecarga()) {
			this.notificarNuevaRecarga(compra);
		}
	}

	/**
	 * Notifica a los observadores la nueva compra recarga
	 * @param compra La compra a notificar.
	 */
	private void notificarNuevaRecarga(Compra compra) {
		this.observadores.stream().forEach(obs -> obs.nuevaCompraRegistrada(this, compra));
	}

	/**
	 * Registra el sem listener dado por par�metro.
	 * @param observer El sem listener a registrar.
	 */
	public void registrarObserver(SEMListener observer) {
		this.observadores.add(observer);
		observer.registrarObservable(this);
	}
	
	/**
	 * Elimina el sem listener dado por par�metro.
	 * @param observer El sem listener a eliminar.
	 */
	public void eliminarObserver(SEMListener observer) {
		this.observadores.remove(observer);
	}

	/**
	 * Registra el estacionamiento dado por par�metro si a�n no se registr�.
	 * @param estacionamiento El estacionamiento a registrar.
	 */
	public void registrarEstacionamiento(Estacionamiento estacionamiento) {
		if (!this.estacionamientosVigentes.contains(estacionamiento)) {
			this.estacionamientosVigentes.add(estacionamiento);
			this.notificarNuevoEstacionamiento(estacionamiento);
		}
	}
	
	/**
	 * Notifica a los observadores el registro de un nuevo estacionamiento.
	 * @param estacionamiento El estacionamiento a notificar.
	 */
	private void notificarNuevoEstacionamiento(Estacionamiento estacionamiento) {
		this.observadores.stream().forEach(obs -> obs.nuevoEstacionamientoRegistrado(this, estacionamiento));
	}

	/** 
	 * Registra el celular con el credito dados por par�metro si a�n no fue registrado el celular.
	 * @param celular El celular a registrar con su credito.
	 * @param credito El credito a registrar con el celular al que corresponde.
	 */
	public void registrarCelularConSuCredito(Celular celular, double credito) {
		if (! estaRegistradoElCelular(celular)) {
			this.creditosPorCelular.add(new CreditoPorCelular(celular, credito));
		}
	}
	
	/**
	 * Describe el siguiente n�mero de compra.
	 * @return El n�mero de la siguiente compra.
	 */
	public int siguienteN�meroDeCompra() {
		return this.compras.size() + 1;
	}

	/**
	 * Finaliza todos los estacionamientos vigentes por fin de franja horaria con la fecha dada por 
	 * par�metro con la hora 20:00.
	 * @param fechaYHoraFinalizaci�n La fecha y hora del fin de la franja horaria del dia.
	 */
	public void finalizarEstacionamientosPorFinDeFranjaHoraria(LocalDateTime fechaYHoraFinalizaci�n) {
		/*
		 * El parametro recibido sera siempre la fecha actual a las 20:00 hs 
		 */
		this.estacionamientosVigentes.stream().forEach(est -> {
			est.finalizar(fechaYHoraFinalizaci�n, this);
			this.notificarFinDeEstacionamiento(est);
		});
		this.estacionamientosHistoricos.addAll(estacionamientosVigentes);
		this.estacionamientosVigentes.clear();
	}
	
	/**
	 * Notifica a los observadores la finalizaci�n del estacionamiento dado.
	 * @param estacionamiento El estacionamiento a notificar.
	 */
	private void notificarFinDeEstacionamiento(Estacionamiento estacionamiento) {
		this.observadores.stream().forEach(obs -> obs.estacionamientoFinalizado(this, estacionamiento));
	}

	/**
	 * Actualiza el credito del celular dado por par�metro con el monto dado.
	 * @param celular El celular a actualizar el credito.
	 * @param monto El monto a sumar en el credito actual del celular.
	 */
	public void actualizarCredito(Celular celular, double monto) {
		int posici�n = 0;
		while (this.creditosPorCelular.get(posici�n).getCelular() != celular) {
			posici�n += 1;
		}
		this.creditosPorCelular.get(posici�n).actualizarCredito(monto);
	}
	
	/**
	 * Finaliza el estacionamiento registrado anteriormente con el celular dado por par�metro con la fecha
	 * y hora dados.
	 * @param celular El celular a finalizar su estacionamiento previo.
	 * @param fechaYHoraFinEstacionamiento La fecha y hora en la que se finaliz� el estacionamiento.
	 */
	public void finalizarEstacionamientoViaApp(Celular celular, LocalDateTime fechaYHoraFinEstacionamiento) {
		EstacionamientoViaApp estacionamientoAFinalizar = this.getEstacionamientoRegistradoConCelular(celular);
		estacionamientoAFinalizar.finalizar(fechaYHoraFinEstacionamiento, this);
		this.notificarFinDeEstacionamiento(estacionamientoAFinalizar);
		this.estacionamientosHistoricos.add(estacionamientoAFinalizar);
		this.estacionamientosVigentes.remove(estacionamientoAFinalizar);
	}
	
	/**
	 * Finaliza el estacionamiento puntual dado por par�metro con la fecha y hora dados.
	 * @param estacionamientoAFinalizar El estacionamiento a finalizar
	 * @param fechaYHoraFinEstacionamiento La fecha y hora en la que se finaliz� el estacionamiento.
	 */
	public void finalizarEstacionamientoPuntual(EstacionamientoPuntual estacionamientoAFinalizar, LocalDateTime fechaYHoraFinEstacionamiento) {
		estacionamientoAFinalizar.finalizar(fechaYHoraFinEstacionamiento, this);
		this.notificarFinDeEstacionamiento(estacionamientoAFinalizar);
		this.estacionamientosHistoricos.add(estacionamientoAFinalizar);
		this.estacionamientosVigentes.remove(estacionamientoAFinalizar);
	}
	
	/** 
	 * Describe el �ltimo estacionamiento finalizado en el sem.
	 * @return El �ltimo estacionamiento finalizado.
	 */
	public Estacionamiento get�ltimoEstacionamientoFinalizado() {
		return this.estacionamientosHistoricos.get(this.estacionamientosHistoricos.size() - 1);
	}
	
	/**
	 * Describe el �ltimo estacionamiento registrado en el sem.
	 * @return El �ltimo estacionamiento registrado.
	 */
	public Estacionamiento get�ltimoEstacionamientoRegistrado() {
		return this.estacionamientosVigentes.get(this.estacionamientosVigentes.size() - 1);
	}
	
	/**
	 * Describe el estacionamiento registrado anteriormente con el celular dado por par�metro.
	 * @param celular El celular a describir su estacionamiento registrado anteriormente.
	 * @return El estacionamiento registrado con el celular dado.
	 */
	private EstacionamientoViaApp getEstacionamientoRegistradoConCelular(Celular celular) {
		int posici�n = 0;
		while (!this.estacionamientosVigentes.get(posici�n).esEstacionamientoRegistradoConCelular(celular)) {
			posici�n += 1;
		}
		return (EstacionamientoViaApp) this.estacionamientosVigentes.get(posici�n);
	}
	
	/**
	 * Describe el precio por hora de estacionamiento del sem.
	 * @return El precio por hora de estacionamiento.
	 */
	public double precioPorHora() {
		return 40.0;
	}
	
	/**
	 * Describe el credito del celular dado por par�metro.
	 * @param celular El celular a describir su credito.
	 * @return El credito del celular dado.
	 */
	public double getCreditoDeCelular(Celular celular) {
		int posici�n = 0;
		while (this.creditosPorCelular.get(posici�n).getCelular() != celular) {
			posici�n += 1;
		}
		return this.creditosPorCelular.get(posici�n).getCredito();
	}
	
	/**
	 * Indica si el celular dado est� registrado en el sem.
	 * @param celular El celular a verificar si est� registrado.
	 * @return Un booleano, verdadero si el celular est� registrado, falso si no.
	 */
	public boolean estaRegistradoElCelular(Celular celular) {
		return this.creditosPorCelular.stream().anyMatch(cpc -> cpc.getCelular() == celular);
	}

	/**
	 * Indica si existe un estacionamiento vigente con la patente dada por par�metro.
	 * @param patente La patente a verificar si existe un estacionamiento vigente.
	 * @return Un booleano, verdadero si existe un estacionamiento con la patente dada, falso si no.
	 */
	public boolean existeEstacionamientoConPatente(String patente) {
		List<Estacionamiento> estacionamientosAVer = this.estacionamientosVigentes;
		while (! estacionamientosAVer.isEmpty() && ! (estacionamientosAVer.get(0).getPatenteDelVehiculo() == patente)) {
			estacionamientosAVer = estacionamientosAVer.subList(1, estacionamientosAVer.size());
		}
		return ! estacionamientosAVer.isEmpty();
	}
	
	/**
	 * Indica si esta vigente el estacionamiento registrado con la patente dada por par�metro a la fecha y 
	 * hora dadas.
	 * @param patente La patente a verificar si su estacionamiento registrado est� vigente.
	 * @param fechaYHoraActual La fecha y hora a verificar si sigue vigente el estacionamiento.
	 * @return Un booleano, verdadero si sigue vigente el estacionamiento con la patente dada a la fecha y hora dadas, falso si no.
	 */
	public boolean estaVigenteElEstacionamientoActualConPatente(String patente, LocalDateTime fechaYHoraActual) {
		return this.estacionamientoActualConPatente(patente).estaVigente(fechaYHoraActual);
	}

	/**
	 * Describe el estacionamiento vigente con la patente dada por par�metro.
	 * @param patente La patente a describir su estacionamiento vigente.
	 * @return El estacionamiento vigente con la patente dada.
	 */
	public Estacionamiento estacionamientoActualConPatente(String patente) {
		List<Estacionamiento> estacionamientosAVer = this.estacionamientosVigentes;
		while (!(estacionamientosAVer.get(0).getPatenteDelVehiculo() == patente)) {
			estacionamientosAVer = estacionamientosAVer.subList(1, estacionamientosAVer.size());
		}
		return estacionamientosAVer.get(0);
	}
	
	/**
	 * Indica si la ubicaci�n del celular dado se encuentra dentro de alguna zona de estacionamiento del sem.
	 * @param celular El celular a verificar si su ubicaci�n est� dentro de alguna zona de estacionamiento.
	 * @return Un booleano, verdadero si la ubicaci�n del celular dado se encuentra dentro de alguna zona de estacionamiento, falso si no.
	 */
	public boolean laUbicaci�nDelCelularSeEncuentraDentroDeUnaZonaDeEstacionamiento(Celular celular) {
		List<ZonaDeEstacionamiento> zonasAVer = this.zonasDeEstacionamiento;
		while (!zonasAVer.isEmpty() && !zonasAVer.get(0).estaEnZona(celular.getUbicaci�n())) {
			zonasAVer = zonasAVer.subList(1, zonasAVer.size());
		}
		return ! zonasAVer.isEmpty();
	}

	/**
	 * Levanta una infracci�n con la patente, la fecha y hora, y la zona dados por par�metro, si debe.
	 * @param patente La patente a levantar una infracci�n.
	 * @param momentoActual La fecha y hora en la que se levanta la infracci�n.
	 * @param zona La zona de estacionamiento donde se verifica la vigencia del estacionamiento
	 */
	public void levantarInfracci�nSiCorresponde(String patente, LocalDateTime momentoActual, ZonaDeEstacionamiento zona) {
		if (this.existeEstacionamientoConPatente(patente) && !this.estaVigenteElEstacionamientoActualConPatente(patente, momentoActual)) {			
			levantarInfracci�n(patente, momentoActual, zona);		
		}
	}

	/**
	 * Levanta una infracci�n con la patente, la fecha y hora, y la zona dados por par�metro
	 * @param patente La patente a levantar una infracci�n.
	 * @param momentoActual La fecha y hora en la que se levanta la infracci�n.
	 * @param zona La zona de estacionamiento donde se verifica la vigencia del estacionamiento
	 */
	private void levantarInfracci�n(String patente, LocalDateTime momentoActual, ZonaDeEstacionamiento zona) {
		Vehiculo vehiculo = estacionamientoActualConPatente(patente).getVehiculo();
		Infracci�n infraccion = new Infracci�n(vehiculo, zona.getInspector(), zona, momentoActual);
		this.infracciones.add(infraccion);
	}

	/**
	 * Describe las zonas de estacionamientos registradas en el sem.
	 * @return Las zonas de estacionamientos del sem.
	 */
	public List<ZonaDeEstacionamiento> getZonasDeEstacionamiento() {
		return this.zonasDeEstacionamiento;
	}

	/**
	 * Describe las compras hechas en el sem.
	 * @return Las compras del sem.
	 */
	public List<Compra> getCompras() {
		return this.compras;
	}

	/**
	 * Describe los estacionamientos vigentes registrados en el sem.
	 * @return Los estacionamientos vigentes del sem.
	 */
	public List<Estacionamiento> getEstacionamientosVigentes() {
		return this.estacionamientosVigentes;
	}
	
	/**
	 * Describe los estacionamientos historicos del sem.
	 * @return Los estacionamientos historicos del sem.
	 */
	public List<Estacionamiento> getEstacionamientosHistoricos() {
		return this.estacionamientosHistoricos;
	}

	/**
	 * Describe los creditos por celular registrados en el sem.
	 * @return Los creditos por celular del sem.
	 */
	public List<CreditoPorCelular> getCreditosPorCelular() {
		return this.creditosPorCelular;
	}

	/**
	 * Describe las infracciones levantadas en el sem.
	 * @return Las infracciones del sem
	 */
	public List<Infracci�n> getInfracciones() {
		return this.infracciones;
	}
	
	/**
	 * Descibre los observadores registrados en el sem.
	 * @return Los observadores del sem.
	 */
	public List<SEMListener> getObservadores() {
		return this.observadores;
	}
	
}
