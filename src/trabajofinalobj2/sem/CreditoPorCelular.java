package trabajofinalobj2.sem;

import trabajofinalobj2.usuario.Celular;

/**
 * Representa los celulares con sus creditos registrados en el sem.
 */

public class CreditoPorCelular {

	private Celular celular;
	private double credito;
	
	/**
	 * Crea un credito por celular.
	 * @param celular El celular a modelar su credito
	 * @param credito El credito del celular.
	 */
	public CreditoPorCelular(Celular celular, double credito) {
		super();
		this.celular = celular;
		this.credito = credito;
	}

	/**
	 * Actualiza el credito del celular sumando el monto dado por parámetro que puede ser positivo o negativo.
	 * @param montoAActualizar El monto a sumar en el credito.
	 */
	public void actualizarCredito(double montoAActualizar) {
		this.credito += montoAActualizar;
	}
	
	/**
	 * Describe el celular.
	 * @return Celular.
	 */
	public Celular getCelular() {
		return this.celular;
	}

	/**
	 * Describe el credito del celular.
	 * @return El credito del celular.
	 */
	public double getCredito() {
		return this.credito;
	}
	
}
