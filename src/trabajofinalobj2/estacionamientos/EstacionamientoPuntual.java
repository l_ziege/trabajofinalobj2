package trabajofinalobj2.estacionamientos;

import java.time.LocalDateTime;

import trabajofinalobj2.usuario.Vehiculo;

/**
 * Representa los estacionamientos en los que se pago por una cantidad determinada de horas.
 * Se extiende de Estacionamiento, quien posee parte de su comportamiento general.
 */

public class EstacionamientoPuntual extends Estacionamiento {
		
	private int horas;
	
	/**
	 * Crea un estacionamiento puntual.
	 * @param vehiculo El vehiculo estacionado.
	 * @param horaInicio La fecha y hora de inicio del estacionamiento.
	 * @param horas Las horas que se estacionará el vehiculo.
	 */
	public EstacionamientoPuntual(Vehiculo vehiculo, LocalDateTime horaInicio, int horas) {
		super();
		this.vehiculo = vehiculo;
		this.inicio = horaInicio;
		this.horas = horas;
	}

	/**
	 * Describe la cantidad de horas
	 * @return un int (la duracion del estacionamiento)
	 */
	@Override
	public int getHoras() {
		return this.horas;
	}
	
	/**
	 * Describe el limite temporal del estacionamiento
	 * @return un LocalDateTime que representa el limite de tiempo del estacionamiento
	 */
	@Override
	public LocalDateTime maximoPosible() {
		return this.inicio.plusHours(this.horas);
	}

	
}
