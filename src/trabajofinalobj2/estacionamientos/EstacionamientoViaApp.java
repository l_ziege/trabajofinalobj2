package trabajofinalobj2.estacionamientos;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import trabajofinalobj2.sem.SEM;
import trabajofinalobj2.usuario.Celular;
import trabajofinalobj2.usuario.Vehiculo;

/**
 * Representa los estacionamientos iniciados en base al saldo del celular con el que inician.
 * Se extiende de Estacionamiento, quien posee parte de su comportamiento general.
 */

public class EstacionamientoViaApp extends Estacionamiento {

	private Celular celular;
	
	/**
	 * Crea un estacionamiento via app.
	 * @param vehiculo El vehiculo estacionado.
	 * @param horaInicio La fecha y hora de inicio del estacionamiento.
	 * @param celular El celular con el que se registro el estacionamiento.
	 */
	public EstacionamientoViaApp(Vehiculo vehiculo, LocalDateTime horaInicio, Celular celular) {
		super();
		this.vehiculo = vehiculo;
		this.inicio = horaInicio;
		this.celular = celular;
	}

	/**
	 * Indica si el estacionamiento se registro con ese celular.
	 * @param celular El celular a verificar si registro el estacionamiento.
	 * @return un booleano que indica si el estacionamiento fue registrado con el celular.
	 */
	@Override
	public boolean esEstacionamientoRegistradoConCelular(Celular celular) {
		return this.celular == celular;
	}

	/**
	 * Describe la cantidad de horas
	 * @return un int (la duracion del estacionamiento)
	 */
	@Override
	public int getHoras() {
		DateTimeFormatter formatoHoras = DateTimeFormatter.ofPattern("HH");
		return Integer.valueOf(this.fin.format(formatoHoras)) - Integer.valueOf(this.inicio.format(formatoHoras));
	}
	
	/**
	 * Cobra el costo del estacionamiento
	 * @param sem El sem donde cobrar el credito.
	 */
	@Override
	protected void cobrarCredito(SEM sem) {
		sem.actualizarCredito(this.celular, -this.costo());
	}
	
	/**
	 * Describe el limite temporal del estacionamiento
	 * @return un LocalDateTime que representa el limite de tiempo del estacionamiento
	 */
	@Override
	public LocalDateTime maximoPosible() {
		int horasMax = (int) (this.celular.consultarSaldo() / new SEM().precioPorHora());
		LocalDateTime m�ximoSeg�nCredito = this.inicio.plusHours(horasMax);
		LocalDateTime m�ximoDelDia = this.inicio.withHour(20).withMinute(0);

		if (m�ximoSeg�nCredito.isAfter(m�ximoDelDia)) {
			return m�ximoDelDia;
		} else {
			return this.inicio.plusHours(horasMax);
		}
	}
}
