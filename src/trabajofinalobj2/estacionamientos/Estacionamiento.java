package trabajofinalobj2.estacionamientos;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import trabajofinalobj2.sem.SEM;
import trabajofinalobj2.usuario.Celular;
import trabajofinalobj2.usuario.Vehiculo;

/**
 * Representacion abstracta de los estacionamientos.
 * Posee el comportamiento que comparten sus subclases: Estacionamiento Manual y Estacionamiento Via App
 */

public abstract class Estacionamiento {

	protected Vehiculo vehiculo;
	protected LocalDateTime inicio;
	protected LocalDateTime fin;

	/**
	 * Describe la fecha de inicio del estacionamiento (d/m/a)
	 * @return Un string con la fecha de inicio
	 */
	public String getFecha() {
		DateTimeFormatter formatoFechas = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		return this.inicio.format(formatoFechas);
	}
	
	/**
	 * Describe la hora de inicio del estacionamiento (h:m)
	 * @return Un string con la hora de inicio
	 */
	public String getHoraInicio() {
		DateTimeFormatter formatoHoras = DateTimeFormatter.ofPattern("HH-mm");
		return this.inicio.format(formatoHoras);
	}
	
	/**
	 * Describe la hora de fin del estacionamiento (h:m)
	 * @return Un string con la hora de fin del estacionamiento
	 */
	public String getHoraFin() {
		DateTimeFormatter formatoHoras = DateTimeFormatter.ofPattern("HH:mm");
		return this.fin.format(formatoHoras);
	}

	/**
	 * Finaliza el estacionamiento en cuestion
	 * @param fechaYHoraFinalización La fecha y hora en la que se finalizó el estacionamiento.
	 * @param sem El sem en el que se realizará el cobro del credito.
	 */
	public void finalizar(LocalDateTime fechaYHoraFinalización, SEM sem) {
		this.fin = fechaYHoraFinalización;
		this.cobrarCredito(sem);
	}
	
	/**
	 * Indica si el estacionamiento se registro con ese celular.
	 * @param celular El celular a verificar si registro el estacionamiento.
	 * @return un booleano que indica si el estacionamiento fue registrado con el celular.
	 */
	public boolean esEstacionamientoRegistradoConCelular(Celular celular) {
		return false;
	}

	/**
	 * Cobra el costo del estacionamiento
	 * @param sem El sem donde cobrar el credito.
	 */
	protected void cobrarCredito(SEM sem) {
		// Por default, es un metodo hook (no hace nada)
	}
	
	/**
	 * Describe la patente del vehiculo
	 * @return un string con la patente
	 */
	public String getPatenteDelVehiculo() {
		return this.vehiculo.getPatente();
	} 

	/**
	 * Describe el vehiculo del estacionamiento
	 * @return un vehiculo
	 */
	public Vehiculo getVehiculo() {
		return this.vehiculo;
	}

	/**
	 * Indica si el estacionamiento esta vigente
	 * @param momentoActual La fecha y hora a verificar si sigue vigente el estacionamiento.
	 * @return un boolean que representa si el estacionamiento esta vigente
	 */
	public boolean estaVigente(LocalDateTime momentoActual) {
		return (this.fin == null) && (this.maximoPosible().isAfter(momentoActual));
	}
	
	/**
	 * Describe la cantidad de horas
	 * @return un int (la duracion del estacionamiento)
	 */
	public abstract int getHoras();
	
	/**
	 * Describe el costo del estacionamiento
	 * @return un double (el costo del estacionamiento)
	 */
	public double costo() {
		return this.getHoras() * (new SEM().precioPorHora());
	}
	
	/**
	 * Describe el limite temporal del estacionamiento
	 * @return un LocalDateTime que representa el limite de tiempo del estacionamiento
	 */
	public abstract LocalDateTime maximoPosible();
		
	/**
	 * Describe la hora limite del estacionamiento (h:m)
	 * @return un string que describe la hora limite del estacionamiento
	 */
	public String getPosibleHoraMáximaFin() {
		DateTimeFormatter formatoHoras = DateTimeFormatter.ofPattern("HH:mm");
		return this.maximoPosible().format(formatoHoras);
	}
	
}
